#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/*
Задача - дана окружность из L клеток. N клеток с известными номерами - закрашены.
Также дано K - количество кусков, на которое мы хотим разрезать нашу окружность.
Пусть после некоторого разреза f(i) - количество закрашенных клеток на i-м куске.
Необходимо разрезать эту окружность так оптимально, чтобы минимизировать max(f(i)) - min(f(i))
Пусть blockSize = L/K;

Вспомогательная структура - MinMax - позволяет поддерживать минимум и максимум на наборе значений
а также изменять значения на +-1 по ключу. Гарантируется, что ключи не очень большие, а значения неотрицательные. 

Идея - есть изначальное разбиение(там где первый блок начинается в индексе 0).
Тогда мы можем рассматривать любое разбиение, как сдвиг изначального разбиения на Y вправо.
Утверждение1 - решим задачу для сдвигов = 0..blockSize-1, затем умножим количество хороших ответов на k.
Утверждение2 - что-то принципиально новое происходит при сдвигах на v[i]%blockSize + 1.

Решение - будем последовательно двигать на v[i]%blockSize и поддерживать в структуре MinMax
количество закрашенных на каждом блоке и минимум и максимум в структуре.
Также будем поддерживать
cnt - количество хороших сдвигов
value - минимальное достигнутое значение max(f(i)) - min(f(i))
id - индекс одного из сдвигов
*/
 
long long ret(long long a, long long b, long long mod) {
	if(a - b == 0) return mod;
	return (a - b + mod) % mod;
}
 
struct MinMax{
	std::vector<int> vals;
	std::vector<int> blocks;
	int min;
	int max;
};
 
void initArr(MinMax* minmax, int vl, int bl) {
	minmax->vals.resize(vl);
	minmax->blocks.resize(bl);
}
 
void initVal(MinMax* minmax) {
	minmax->min = 1e9;
	minmax->max = -1e9;
	for (auto t : minmax->blocks) {
		minmax->min = min(minmax->min, t);
		minmax->max = max(minmax->max, t);
	} 
}
 
void increase(MinMax* minmax, int blockId) {
	int value = minmax->blocks[blockId];
	if (--minmax->vals[value] == 0 && value == minmax->min) {
		minmax->min++;
	}
	minmax->max = max(minmax->max, value + 1);
	++minmax->vals[value + 1];
	++minmax->blocks[blockId];
}
 
void decrease(MinMax* minmax, int blockId) {
	int value = minmax->blocks[blockId];
	if (--minmax->vals[value] == 0 && value == minmax->max) {
		minmax->max--;
	}
	minmax->min = min(minmax->min, value - 1);
	++minmax->vals[value - 1];
	--minmax->blocks[blockId];
}
 
main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0, k = 0;
	long long l = 0;
	std::cin >> l >> n >> k;
	if (n == 0) {
		std::cout << "0 " << l << "\n" << 1;
		return 0;
	}
	long long blockSize = l / k;
	std::vector<long long> v(n);
	for (auto &t : v) {
		cin >> t;
		--t;
	}
	
	std::vector<long long> buf;
	for (auto t : v) {
		buf.push_back(t % blockSize);
	}
	std::sort(buf.begin(), buf.end());
	std::vector<std::pair<long long, std::vector<int>>> pos;
	for (auto t : buf) {
		if (pos.empty() || pos.back().first != t) {
			pos.push_back({t, std::vector<int>()});
		}
	} 
	
	for (auto t : v) {
		long long pos0 = t % blockSize;
		int l = 0;
		int r = pos.size();
		while (l + 1 < r) {
			int m = (l + r) / 2;
			if (pos[m].first <= pos0) {
				l = m;
			} else {
				r = m;
			}
		}
		pos[l].second.push_back(t / blockSize);
	}
	pos.push_back(pos[0]); // make it cycled
	
	MinMax mm = MinMax();
	initArr(&mm, 2 * n + 1, k);
	for (auto t : v) {
		++mm.blocks[t / blockSize];	
	}
	for (auto t : mm.blocks) {
		++mm.vals[t];
	}
	initVal(&mm);
 
	int value = 1e9;
	long long cnt = 0;
	long long id = -1;
	long long last = -1;
	
	long long fs = pos[0].first;
	for (auto pos0 : pos) {		
		if (last != -1) {
			int Value = mm.max - mm.min;
			if (Value < value) {
				value = Value;
				cnt = ret(pos0.first, last, blockSize);
				id = pos0.first + ((pos0.first == 0) ? blockSize : 0);
			} else if(Value == value) {
				cnt += ret(pos0.first, last, blockSize);
			}
		}
		if (last == -1 || fs != pos0.first) {
			for (auto t : pos0.second) {
				int to = (t - 1 + k) % k;
				increase(&mm, to);
			}
			for (auto t : pos0.second) {
				int from = t;
				decrease(&mm, from);
			}	
		}
		last = pos0.first;
	}
	std::cout << value << " " << cnt * k << "\n" << id; 
}
