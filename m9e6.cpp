#include <iostream>
#include <vector>
#include <set>
#include <limits>

/*
Задача:
Дан взвешенный неориентированный граф.
Требуется найти вес минимального пути между двумя вершинами.

Входные данные
Первая строка входного файла содержит два натуральных числа n и m —
количество вершин и ребер графа соответственно.
Вторая строка входного файла содержит натуральные числа s и t —
номера вершин, длину пути между которыми требуется найти (1≤s,t≤n, s≠t).

Следующие m строк содержат описание ребер по одному на строке.
Ребро номер i описывается тремя натуральными числами bi, ei и wi —
номера концов ребра и его вес соответственно (1≤bi,ei≤n, 0≤wi≤100).

n≤100000, m≤200000.

Выходные данные
Первая строка выходного файла должна содержать одно натуральное число —
вес минимального пути между вершинами s и t.

Если путь из s в t не существует, выведите -1.
*/

struct Edge{
    int to;
    int cost;
    Edge(int to, int cost): to(to), cost(cost) {}
};

class Graph {
public:
    explicit Graph(int size) {
        gr.resize(size, std::vector<Edge>());
    }
    void addEdge(int a, int b, int cost) {
        gr[a].push_back(Edge(b, cost));
        gr[b].push_back(Edge(a, cost));
    }
    int size() const {
        return gr.size();
    }
    const std::vector<Edge>& getNeighbours(int v) const {
        return gr[v];
    }
private:
    std::vector<std::vector<Edge>> gr;
};

// Поиск расстояния между вершинами Дейкстрой
int findDistance(const Graph& graph, int s, int f) {
    const int inf = std::numeric_limits<int>::max();
    std::vector<int> dist(graph.size(), inf);
    dist[s] = 0;
    std::set<std::pair<int,int>> st = {std::make_pair(dist[s], s)};
    while (!st.empty()) {
        auto t = *st.begin();
        st.erase(t);
        for (auto edge: graph.getNeighbours(t.second)) {
            if (dist[edge.to] > t.first + edge.cost) {
                st.erase({dist[edge.to], edge.to});
                dist[edge.to] = t.first + edge.cost;
                st.insert({dist[edge.to], edge.to});
            }
        }
    }
    return dist[f];
}

int main() {
    int n = 0, m = 0, s = 0, f = 0;
    std::cin >> n >> m >> s >> f;
    Graph g(n);
    for (int i = 0; i < m; i++) {
        int a = 0, b = 0, cost = 0;
        std::cin >> a >> b >> cost;
        g.addEdge(a - 1, b - 1, cost);
    }
    int ans =  findDistance(g, s - 1, f - 1);
    std::cout << (ans == std::numeric_limits<int>::max() ? -1 : ans);
}
