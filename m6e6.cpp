#include <iostream>
#include <vector>

/*
Даны две последовательности. Найдите длину их наибольшей общей подпоследовательности (подпоследовательность — это то, что можно получить из данной последовательности вычеркиванием некоторых элементов).

Входные данные
В первой строке входного файла записано число N — длина первой последовательности (1⩽N⩽1000). Во второй строке записаны члены первой последовательности (через пробел) — целые числа, не превосходящие 10 000 по модулю. В третьей строке записано число M — длина второй последовательности (1⩽M⩽1000). В четвертой строке записаны члены второй последовательности (через пробел) — целые числа, не превосходящие 10 000 по модулю.

Выходные данные
В выходной файл требуется вывести единственное целое число: длину наибольшей общей подпоследовательности, или число 0, если такой не существует.

Идея: dp[i][j] - ответ для префиксов i и j соответственно 1ой и 2ой строки.
Пересчёт из dp[i-1][j], dp[i][j-1], dp[i-1][j-1]
*/

int findMCS(const std::vector<int>& a, const std::vector<int>& b) {
	int n = a.size(), m = b.size();
	std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1, -1));
	dp[0][0] = 0;
	for (int i = 0; i <= n; i++) {
		for (int j = 0 + (i == 0); j <= m; j++) {
			if (i != 0) {
				if (j != 0) {
					dp[i][j] = std::max(dp[i][j], (a[i - 1] == b[j - 1]) + dp[i - 1][j - 1]);
				}
                dp[i][j] = std::max(dp[i][j], dp[i - 1][j]);
			}
			if (j != 0) {
				dp[i][j] = std::max(dp[i][j], dp[i][j-1]);
            }
		}
	}
	return dp[n][m];
}

int main() {
	int n = 0, m = 0;
	std::cin >> n;
	std::vector<int> a(n), b;
	for (auto &t : a) {
		std::cin >> t;
    }
	std::cin >> m;
	b.resize(m);
	for (auto &t : b) {
		std::cin >> t;
    }
	std::cout << findMCS(a, b);
}
