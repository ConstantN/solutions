#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

/*
Дан неориентированный граф без петель и кратных рёбер. Требуется найти все точки сочленения в нем.

Входные данные
Первая строка входного файла содержит два натуральных числа n и m — количество вершин и ребер графа соответственно (n≤20000,m≤200000).

Следующие m строк содержат описание ребер по одному на строке. Ребро номер i описывается двумя натуральными числами bi,ei — номерами концов ребра (1≤bi,ei≤n).

Выходные данные
Первая строка выходного файла должна содержать одно натуральное число k — количество точек сочленения в заданном графе. На следующей строке выведите k целых чисел — номера вершин, которые являются точками сочленения, в возрастающем порядке.

Решение:
Реализация стандартного алгоритма.
*/

class Graph{
public:
    explicit Graph(int n): edges(n, std::vector<int>()) {}
    void addEdge(int a, int b) {
        edges[a].push_back(b);
        edges[b].push_back(a);
    } 
    const std::vector<int>& getNeighbours(int v) const {
        return edges[v];
    }
    int getSize() const {
        return edges.size();
    }
private:
    std::vector<std::vector<int>> edges; 
};

class GraphProcessor {
public:
    explicit GraphProcessor(const Graph& graph): graph(graph), timer(0), 
                                used(graph.getSize(), 0), tin(graph.getSize(), 0), fup(graph.getSize(), 0) {}
    void findJointPoints(std::vector<int>& ans) {
        for (int i = 0; i < graph.getSize(); i++) {
            if (!isUsed(i)) {
                findPoints(i, -1);
            }
        }
        returnJointPoints(ans);
    }
private:
    const Graph& graph;
    int timer;
    std::vector<bool> used;
    std::vector<int> tin;
    std::vector<int> fup; 
    std::set<int> jointPoints;
    bool isUsed(int v) {
        return used[v];
    }
    void findPoints(int v, int p) {
        used[v] = 1;
        fup[v] = tin[v] = timer++;
        int sonCounter = 0;
        for (auto t: graph.getNeighbours(v)) {
            if (t == p) {
                continue;
            }
            if (used[t]) {
                fup[v] = std::min(fup[v], tin[t]);
                continue;
            } 
            findPoints(t, v);
            if (fup[t] >= tin[v] && p != -1) {
                jointPoints.insert(v);
            }
            sonCounter++;
            fup[v] = std::min(fup[v], fup[t]);
        }
        if (p == -1 && sonCounter > 1) {
            jointPoints.insert(v);
        }
    }
    void returnJointPoints(std::vector<int>& ans) {
        for (auto t: jointPoints) {
            ans.push_back(t);
        }
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0, m = 0;
    std::cin >> n >> m; 
    Graph g(n);
    for (int i = 0; i < m; i++) {
        int a = 0, b = 0;
        std::cin >> a >> b;
        g.addEdge(a - 1, b - 1);
    }
    std::vector<int> ans;
    GraphProcessor(g).findJointPoints(ans);
    std::cout << ans.size() << "\n";
    for (auto t : ans) {
        std::cout << t + 1 << " "; 
    }
}
