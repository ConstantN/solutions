#include <iostream>
#include <vector>

/*
Реализуйте ассоциативный массив с использованием хеш-таблицы. Использовать стандартную библиотеку (set, map, LinkedHashMap, и т. п.) не разрешается.

Входные данные
Входной файл содержит описание операций, их количество не превышает 100000. В каждой строке находится одна из следующих операций:

put x y — поставить в соответствие ключу x значение y. Если ключ уже есть, то значение необходимо изменить.
delete x — удалить ключ x. Если элемента x нет, то ничего делать не надо.
get x — если ключ x есть в ассоциативном массиве, то выведите соответствующее ему значение, иначе выведите «none».
Ключи и значения — строки из латинских букв длинной не более 20 символов.

Использован метод цепочек.
*/

struct Item {
    std::string key;
    std::string value;
    Item(const std::string& key, const std::string& value): key(key), value(value) {}
};
 
struct Id {
    int hash;
    int index;
    Id(int hash, int index): hash(hash), index(index) {}
};

class CustomMap {
public:
    const std::string DEFAULT_VALUE = "none";
    CustomMap(): hash_table(size, std::vector<Item>()) {}
    std::string get(const std::string& key) {
        Id id = find(key);
        if (id.index == hash_table[id.hash].size()) {
            return DEFAULT_VALUE;
        }
        return hash_table[id.hash][id.index].value;
    }
    void erase(const std::string& key) {
        Id id = find(key);
        if (id.index == hash_table[id.hash].size()) {
            return;
        }
        hash_table[id.hash].erase(hash_table[id.hash].begin() + id.index);
    }
    void put(const std::string& key, const std::string& value) {
        Id id = find(key);
        if (id.index == hash_table[id.hash].size()) {
            hash_table[id.hash].push_back(Item(key, DEFAULT_VALUE));
        }
        hash_table[id.hash][id.index].value = value;
    }
private:
    const long long mod = 1e9 + 7;
    const long long p = 101;
    const int size = 49999;
    std::vector<std::vector<Item>> hash_table;
    int h(const std::string& s) {
        long long ret = 0;
        long long P = 1;
        for (int i = 0; i < s.length(); i++) {
            ret = (ret + s[i] * P) % mod;
            P = (P * p) % mod;
        }
        return ret % size;
    }
    Id find(const std::string& s) {
        int hash = h(s);
        int i;
        for (i = 0; i < hash_table[hash].size(); i++) {
            if (hash_table[hash][i].key == s) {
                break;
            }
        }
        return Id(hash, i);
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    freopen("map.in", "r", stdin);
    freopen("map.out", "w", stdout);
    std::string q = "";
    CustomMap mp = CustomMap();
    while (std::cin >> q) {
        std::string key;
        std::cin >> key;
        if (q == "put"){
            std::string value;
            std::cin >> value;
            mp.put(key, value);
        } else if(q == "delete") {
            mp.erase(key);
        } else if(q == "get") {
            std::cout << mp.get(key) << "\n";
        }
    }
}
