#include <iostream>

/*
Решение на два балла!

Задача:
Нужно отвечать на запросы вида
• + x – добавить в мультимножество число x.
• ? x – посчитать сумму чисел не больших x.
Формат входных данных
В первой строке содержится число запросов 1 6 q 6 105
.
Далее каждая строка содержит один запрос.
Все числа x целые от 0 до 109 − 1.

Решение: динамически строящееся дерево отрезков. Пусть мы его строим на массиве int mas[1e9]
mas[i] - количество чисел i в мультимножестве.
Ответ на первый запрос - обновление в точке
на второй - сумма на отрезке
*/

class SegmentTree {	
public:
	SegmentTree(int leftBorder, int rightBorder);
	long long getSum(int l, int r);
	void addValue(int ind);
	~SegmentTree() {
		if(l) delete l;
		if(r) delete r;
	}
private:
	SegmentTree* l;
	SegmentTree* r;
	int leftBorder;
	int rightBorder;
	int midIndex;
	long long sum;
	void makeSons();
};

void SegmentTree::makeSons() {
	if (!l)
		l = new SegmentTree(leftBorder, midIndex);
	if (!r)
		r = new SegmentTree(midIndex + 1, rightBorder);
}

SegmentTree::SegmentTree(int leftBorder, int rightBorder): leftBorder(leftBorder), rightBorder(rightBorder), midIndex((leftBorder + rightBorder)/2), sum(0) {
	l = r = nullptr;
}

long long SegmentTree::getSum(int L, int R) {
	if (L > R)
		return 0;
	if (leftBorder == L && rightBorder == R)
		return sum;
	makeSons();
	return l->getSum(L, std::min(midIndex, R)) + r->getSum(std::max(midIndex + 1, L), R);
}

void SegmentTree::addValue(int ind) {
	sum += ind;
	if (leftBorder == rightBorder)
		return;
	makeSons();
	if (ind <= midIndex)
		l->addValue(ind);
	else
		r->addValue(ind);
}

int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	SegmentTree root = SegmentTree(0, 1e9);
	int n = 0;
	std::cin >> n;
	for (int i = 0; i < n; ++i) {
		char c;
		int v;
		std::cin >> c >> v;
		if (c == '+') 
			root.addValue(v);
		else
			std::cout << root.getSum(0,v) << std::endl;
	}
}
