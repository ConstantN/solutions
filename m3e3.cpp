#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <string>

using std::cin;
using std::cout;

/*
Напишите программу, реализующую структуру данных, позволяющую добавлять и удалять элементы, а также находить k-й максимум.

Входные данные
Первая строка входного файла содержит натуральное число n — количество команд (n≤100000). Последующие n строк содержат по одной команде каждая. Команда записывается в виде двух чисел ci и ki — тип и аргумент команды соответственно (|ki|≤109).

Поддерживаемые команды:

+1 (или просто 1): Добавить элемент с ключом ki.
   0: Найти и вывести ki-й максимум.
−1: Удалить элемент с ключом ki.
Гарантируется, что в процессе работы в структуре не требуется хранить элементы с равными ключами или удалять несуществующие элементы. Также гарантируется, что при запросе ki-го максимума, он существует.
*/

struct Node {
    int val;
    int prior;
    int size;
    Node* l;
    Node* r;
    Node(int val): val(val), size(1), l(nullptr), r(nullptr) {
        prior = (rand() << 15) | rand();
    }
    static int getSize(Node* a);
    static void updateSize(Node* a);
    ~Node() {
	delete l;
	delete r;
    }
};

int Node::getSize(Node* a) {
    if(!a)
        return 0;
    return a->size;
}

void Node::updateSize(Node *a) {
    if(!a)
        return;
    a->size = getSize(a->l) + getSize(a->r) + 1;
}

class Treap {
public:
    Treap(): root(nullptr) {}
    void add(int value) {
        add(root, value);
    }
    void erase(int value) {
        erase(root, value);
    }
    int getKth(int k) {
        return getKth(root, k);
    }
    ~Treap() {
	delete root;
    }
private:
    Node* root;
    static Node* merge(Node* a, Node* b);
    static std::pair<Node*, Node*> splitValR(Node* a, int val); //same of Val are in R
    static std::pair<Node*, Node*> splitValL(Node* a, int val); //same of Val are in L
    static std::pair<Node*, Node*> splitVal(Node* a, int val, int flag);
    static std::pair<Node*, Node*> splitSize(Node* a, int size);
    static void add(Node*& root, int value);
    static void erase(Node*& root, int value);
    static int getKth(Node*& root, int k);
};

Node* Treap::merge(Node *a, Node *b) {
    if (!a)
        return b;
    if(!b)
        return a;
    if (a->prior < b->prior) {
        a->r = merge(a->r, b);
        Node::updateSize(a);
        return a;
    } else {
        b->l = merge(a, b->l);
        Node::updateSize(b);
        return b;
    }
}

std::pair<Node*, Node*> Treap::splitValL(Node* a, int val) {
    return splitVal(a, val, 0);
}
std::pair<Node*, Node*> Treap::splitValR(Node* a, int val) {
    return splitVal(a, val, 1);
}

std::pair<Node*, Node*> Treap::splitVal(Node* a, int val, int flag) {
    if (!a)
        return {nullptr, nullptr};
    if ((flag == 1 && a->val > val) || (flag == 0 && a->val >= val)) {
        auto t = splitVal(a->r, val, flag);
        a->r = t.first;
        Node::updateSize(a);
        return {a, t.second};
    } else {
        auto t = splitVal(a->l, val, flag);
        a->l = t.second;
        Node::updateSize(a);
        return {t.first, a};
    }
}

std::pair<Node*, Node*> Treap::splitSize(Node* a, int size) {
    if (!a)
        return {nullptr, nullptr};
    if (Node::getSize(a->l) < size) {
        auto t = splitSize(a->r, size - Node::getSize(a->l) - 1);
        a->r = t.first;
        Node::updateSize(a);
        return {a, t.second};
    } else {
        auto t = splitSize(a->l, size);
        a->l = t.second;
        Node::updateSize(a);
        return {t.first, a};
    }
}

void Treap::add(Node*& root, int value) {
    auto t = splitValR(root, value);
    root = merge(t.first, merge(new Node(value), t.second));
}

void Treap::erase(Node*& root, int value) {
    auto t = splitValR(root, value);
    auto t1 = splitSize(t.second, 1);
    root = merge(t.first, t1.second);
}

int Treap::getKth(Node*& root, int k) {
    auto t = splitSize(root, k - 1);
    auto t1 = splitSize(t.second, 1);
    int value = t1.first->val;
    root = merge(t.first, merge(t1.first, t1.second));
    return value;
}

main() {
    std::ios::sync_with_stdio(false);
    cin.tie(0);
    srand(time(0));
    int n = 0;
    cin >> n;
    Treap treap = Treap();
    while (n--) {
        int c = 0, k = 0;
        cin >> c >> k;
        if (c == 1) {
            treap.add(k);
        } else if (c == -1) {
            treap.erase(k);
        } else if (c == 0) {
            cout << treap.getKth(k) << "\n";
        }
    }
    return 0;
}
