#include <iostream>
#include <vector>
#include <list>
#include <cmath>
#include <cassert>

template <typename T, typename AllocType = std::allocator<T>>
class List {
private:
    template<bool IsConst>
    struct common_iterator;
    struct Node;
    using NodeAllocType = typename std::allocator_traits<AllocType>::template rebind_alloc<Node>;
    using AllocTraits = std::allocator_traits<NodeAllocType>;
public:
    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    List(const AllocType& allocator = AllocType()): allocator_(allocator) {
        end_ = AllocTraits::allocate(allocator_, 1);
        end_->prev = end_->next = end_;
        size_ = 0;
    }

    List(size_t count, const T& value, const AllocType& allocator = AllocType()): List(allocator) {
        for (size_t i = 0; i < count; ++i) {
            push_back(value);
        }
    }

    List(size_t count, const AllocType& allocator = AllocType()): List(allocator) {
        for (size_t i = 0; i < count; ++i) {
            auto iter = --end();
            ++size_;
            Node* new_node = AllocTraits::allocate(allocator_, 1);
            AllocTraits::construct(allocator_, new_node);
            Node* node_next = const_cast<Node*>(iter.node);
            Node* node_prev = node_next->prev;
            new_node->prev = node_prev;
            new_node->next = node_next;
            node_prev->next = node_next->prev = new_node;
        }
    }

    List(const List& list) {
        allocator_ = AllocTraits::select_on_container_copy_construction(list.allocator_);
        end_ = AllocTraits::allocate(allocator_, 1);
        end_->prev = end_->next = end_;
        size_ = 0;
        for (auto t = list.begin(); t != list.end(); ++t) {
            push_back(*t);
        }
    }

    List(List&& list): end_(std::move(list.end_)), size_(std::move(list.size_)), allocator_(list.allocator_) {
        list.end_ = nullptr;
        list.size_ = 0;
    }

    ~List() {
        while(size() > 0) {
            pop_back();
        }
        AllocTraits::deallocate(allocator_, end_, 1);
    }

    List& operator=(const List& list) {
        if (AllocTraits::propagate_on_container_copy_assignment::value && allocator_ != list.allocator_) {
            allocator_ = list.allocator_;
        }
        while (size() > list.size()) {
            pop_back();
        }
        auto j = list.begin();
        for (auto i = begin(); i != end() && j != list.end(); ++i, ++j) {
            (*i) = (*j);
        }
        for (; j != list.end(); ++j) {
            push_back(*j);
        }
        return *this;
    }

    List& operator=(List&& list) {
        List temp = std::move(list);
        std::swap(end_, temp.end_);
        std::swap(size_, temp.size_);
        return *this;
    }

    AllocType get_allocator() {
        return allocator_;
    }

    size_t size() const {
        return size_;
    }

    void push_back(const T& value) {
        insert(end(), value);
    }

    void push_back(T&& value) {
        insert(end(), std::move(value));
    }

    void push_front(const T& value) {
        insert(begin(), value);
    }

    void pop_back() {
        erase(--end());
    }

    void pop_front() {
        erase(begin());
    }

    void insert(const_iterator iter, const T& value) { 
        Node* new_node = AllocTraits::allocate(allocator_, 1);
        AllocTraits::construct(allocator_, new_node, value);
        Node* node_next = const_cast<Node*>(iter.node);
        Node* node_prev = node_next->prev;
        new_node->prev = node_prev;
        new_node->next = node_next;
        node_prev->next = node_next->prev = new_node;
        ++size_;
    }

    void insert(iterator iter, T&& value) { 
        Node* new_node = AllocTraits::allocate(allocator_, 1);
        AllocType alloc = allocator_;
        std::allocator_traits<AllocType>::construct(alloc, &(new_node->value), std::move(const_cast<std::remove_const_t<decltype(value.first)>&&>(value.first)), std::move(value.second));
        Node* node_next = iter.node;
        Node* node_prev = node_next->prev;
        new_node->prev = node_prev;
        new_node->next = node_next;
        node_prev->next = node_next->prev = new_node;
        ++size_;
    }

    void erase(const_iterator iter) { 
        Node* node = const_cast<Node*>(iter.node);
        Node* node_next = node->next;
        Node* node_prev = node->prev;
        node_next->prev = node_prev;
        node_prev->next = node_next;
        AllocTraits::destroy(allocator_, node);
        AllocTraits::deallocate(allocator_, node, 1);
        --size_;
    }

    template<class... Args>
    void emplace_back(Args&&... args) {
        Node* temp = AllocTraits::allocate(allocator_, 1);
        AllocType alloc = allocator_;
        std::allocator_traits<AllocType>::construct(alloc, &(temp->value), std::forward<Args>(args)...);
        temp->prev = end_->prev;
        temp->next = end_;
        end_->prev->next = temp;
        end_->prev = temp;
        ++size_;
    }

    void insertAfter(iterator object, iterator pos) {
        object.node->prev->next = object.node->next;
        object.node->next->prev = object.node->prev;
        object.node->prev = pos.node;
        object.node->next = pos.node->next;
        pos.node->next->prev = object.node;
        pos.node->next = object.node;
    }

    iterator begin() {
        return iterator(end_->next);
    }

    iterator end() {
        return iterator(end_);
    }

    const_iterator begin() const {
        return const_iterator(end_->next);
    }

    const_iterator end() const {
        return const_iterator(end_);
    }

    const_iterator cbegin() const {
        return const_iterator(end_->next);
    }

    const_iterator cend() const {
        return const_iterator(end_);
    }

    reverse_iterator rbegin() {
        return std::make_reverse_iterator(end());
    }

    reverse_iterator rend() {
        return std::make_reverse_iterator(begin());
    }

    const_reverse_iterator rbegin() const {
        return std::make_reverse_iterator(cend());
    }

    const_reverse_iterator rend() const {
        return std::make_reverse_iterator(cbegin());
    }

    const_reverse_iterator crbegin() const {
        return std::make_reverse_iterator(cend());
    }

    const_reverse_iterator crend() const {
        return std::make_reverse_iterator(cbegin());
    }

private:
    Node* end_;
    size_t size_;
    NodeAllocType allocator_;

    struct Node {
        T value;
        Node* next;
        Node* prev;
        Node(): value() {
            next = prev = nullptr;
        }
        Node(const T& value): value(value) {
            next = prev = nullptr;
        }
        Node(T&& value): value(std::move(value)) {
            next = prev = nullptr;
        }
    };

    template<bool IsConst>
    struct common_iterator {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = typename std::conditional<IsConst, const T, T>::type;
        using pointer = typename std::conditional<IsConst, const T*, T*>::type;
        using reference = typename std::conditional<IsConst, const T&, T&>::type;
        using iterator_category = std::bidirectional_iterator_tag;
        common_iterator(Node* node): node(node) {}
        reference operator*() const {
            return node->value;
        }
        pointer operator->() const {
            return &node->value;
        }
        common_iterator& operator++() {
            node = node->next;
            return *this;
        }
        common_iterator operator++(int) {
            common_iterator ret = *this;
            ++(*this);
            return ret;
        }
        common_iterator& operator--() {
            node = node->prev;
            return *this;
        }
        common_iterator operator--(int) {
            common_iterator ret = *this;
            --(*this);
            return ret;
        }
        bool operator==(const common_iterator& iter) const {
            return node == iter.node;
        }
        bool operator!=(const common_iterator& iter) const {
            return !(*this == iter);
        }
        operator common_iterator<true>() const {
            return common_iterator<true>(node);
        }
        friend common_iterator<true>;
        friend common_iterator<false>;
        friend List;
    private:
        Node* node;
    };
};


template <typename Key, typename Value, typename Hash = std::hash<Key>,
        typename Equal = std::equal_to<Key>, typename Allocator = std::allocator<std::pair<const Key, Value>>>
class UnorderedMap {
public:
    using NodeType = std::pair<const Key, Value>;

private:
    using NodeAllocator = typename std::allocator_traits<Allocator>::template rebind_alloc<NodeType>;
    using ListType = List<NodeType, NodeAllocator>;
    using ListIterator = typename ListType::iterator;
    using ListConstIterator = typename ListType::const_iterator;
    using ListIteratorAllocator = typename std::allocator_traits<Allocator>::template rebind_alloc<ListIterator>;
    using VectorType = std::vector<ListIterator, ListIteratorAllocator>;

public:
    using iterator = ListIterator;
    using const_iterator = ListConstIterator;
    UnorderedMap(): MLfactor(1) {
        reserve(1000);
    }
    UnorderedMap(const UnorderedMap& unorderedMap): heads(unorderedMap.heads.size(), end()), MLfactor(unorderedMap.MLfactor) {
        for (auto& node: unorderedMap) {
            insert(node);
        }
    }
    UnorderedMap& operator=(const UnorderedMap& unorderedMap) {
        while (size() > 0) {
            erase(begin());
        }
        MLfactor = unorderedMap.MLfactor;
        heads.resize(unorderedMap.heads.size(), end());
        for (auto& node: unorderedMap) {
            insert(node);
        }
        return *this;
    }
    UnorderedMap(UnorderedMap&&) = default;
    UnorderedMap& operator=(UnorderedMap&& unorderedMap) {
        list = std::move(unorderedMap.list);
        heads = std::move(unorderedMap.heads);
        MLfactor = unorderedMap.MLfactor;
        return *this;
    }
    ~UnorderedMap() = default;
    Value& operator[](const Key& key) {
        try {
            return at(key);
        } catch (...) {
            emplace(key, Value());
            return at(key);
        };
    }
    Value& at(const Key& key) {
        iterator it = find(key);
        if (it == end()) {
            throw std::out_of_range("");
        }
        return it->second;
    }
    const Value& operator[](const Key& key) const {
        try {
            return at(key);
        } catch (...) {
            emplace(key, Value());
            return at(key);
        };
    }
    const Value& at(const Key& key) const {
        const_iterator it = find(key);
        if (it == cend()) {
            throw std::out_of_range("");
        }
        return it->second;
    }
    iterator find(const Key& key) {
        auto hash = local_hash(key);
        iterator iter = heads[hash];
        while (iter != end() && local_hash(iter->first) == hash && !Equal()(iter->first, key)) {
            ++iter;
        }
        if (iter == end() || local_hash(iter->first) != local_hash(key)) {
            return end();
        }
        return iter;
    }
    const_iterator find(const Key& key) const {
        auto hash = local_hash(key);
        const_iterator iter = heads[hash];
        while (iter != cend() && local_hash(iter->first) == local_hash(key) && !Equal()(iter->first, key)) {
            ++iter;
        }
        if (iter == cend() || local_hash(iter->first) != local_hash(key)) {
            return cend();
        }
        return iter;
    }
    std::pair<iterator, bool> insert(const NodeType& node) {
        return emplace(node);
    }
    std::pair<iterator, bool> insert(NodeType&& node) {
        list.push_back(std::move(node));
        NodeType& keyValue = *list.rbegin();
        iterator iter = find(keyValue.first);
        if (iter == --end()) {
            return {--end(), true};
        }
        if (iter != end()) {
            list.pop_back();
            return {iter, false};
        }
        auto hash = local_hash(keyValue.first);
        if (heads[hash] == end()) {
            heads[hash] = --end();
            return {--end(), true};
        }
        list.insertAfter(--end(), heads[hash]);
        capacityCure();
        return {std::next(heads[hash]), true};
    }
    template<typename InputIterator>
    void insert(InputIterator from, InputIterator to) {
        for (auto it = from; it != to; ++it) {
            insert(*it);
        }
    }
    template<class... Args>
    std::pair<iterator, bool> emplace(Args&&... args) {
        list.emplace_back(std::forward<Args>(args)...);
        NodeType& keyValue = *list.rbegin();
        iterator iter = find(keyValue.first);
        if (iter == --end()) {
            return {--end(), true};
        }
        if (iter != end()) {
            list.pop_back();
            return {iter, false};
        }
        auto hash = local_hash(keyValue.first);
        if (heads[hash] == end()) {
            heads[hash] = --end();
            return {--end(), true};
        }
        list.insertAfter(--end(), heads[hash]);
        capacityCure();
        return {std::next(heads[hash]), true};
    }
    iterator erase(const Key& key) {
        iterator iter = find(key);
        if (iter == end()) {
            return end();
        }
        auto hash = local_hash(key);
        if (heads[hash] == iter) {
            if (std::next(heads[hash]) == end() || local_hash(std::next(heads[hash])->first) != local_hash(heads[hash]->first)) {
                heads[hash] = end();
            } else {
                ++heads[hash];
            }
        }
        iterator next_it = iter;
        ++next_it;
        list.erase(iter);
        return next_it;
    }
    iterator erase(iterator it) {
        return erase(it->first);
    }
    iterator erase(iterator from, iterator to) {
        for (auto it = from; it != to; it = erase(it)) {}
        return to;
    }
    iterator begin() {
        return list.begin();
    }
    const_iterator begin() const {
        return cbegin();
    }
    iterator end() {
        return list.end();
    }
    const_iterator end() const {
        return cend();
    }
    const_iterator cbegin() const {
        return list.cbegin();
    }
    const_iterator cend() const {
        return list.cend();
    }
    int size() const {
        return list.size();
    }
    float load_factor() const {
        return 1.0f * size() / heads.size();
    }
    void max_load_factor(float value) {
        MLfactor = value;
    }
    float max_load_factor() const {
        return MLfactor;
    }
    void reserve(int elemCount) {
        if (ceil(elemCount / MLfactor) > heads.size()) {
            heads.assign(ceil(elemCount / MLfactor), end());
            rehash();
        }
    }

private:
    ListType list;
    VectorType heads;
    float MLfactor;
    const float expansionCoef = 2;
    const int expansionAddition = 10;
    size_t local_hash(const Key& key) const {
        return Hash()(key) % heads.size();
    }
    void capacityCure() {
        if (load_factor() > max_load_factor()) {
            reserve(size() * expansionCoef + expansionAddition);
        }
    }
    void rehash() {
        for (auto it = begin(); it != end();) {
            auto nxt = std::next(it);
            auto hash = local_hash((*it).first);
            if (heads[hash] != end()) {
                list.insertAfter(it, heads[hash]);
            } else {
                heads[hash] = it;
            }
            it = nxt;
        }
    }
};
