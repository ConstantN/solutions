#include <set>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>

using std::cin;
using std::cout;

/*
Задача:
Вы должны реализовать следующие операции на структуре:

ADD e s
Добавить в множество №s (0≤s≤m) число e (0≤e≤n).

DELETE e s
Удалить из множества №s (0≤s≤m) число e (0≤e≤n). Гарантируется, что до этого число e было помещено в множество

CLEAR s
Очистить множество №s (0≤s≤m).

LISTSET s
Показать содержимое множества №s (0≤s≤m) в возрастающем порядке, либо −1, если множество пусто.

LISTSETSOF e
Показать множества, в которых лежит число e (0≤e≤n), либо −1, если этого числа нет ни в одном множестве.

Решение: Для каждого множества создадим set с индексами элементов, которые там лежат
Для каждого элемента создадим set с индексами множеств, в которых этот элемент лежит
*/

void makeLonger(std::string &num) {
	reverse(num.begin(), num.end());
	while (num.size() < 20)
		num += '0';
	reverse(num.begin(), num.end());			
}

main() {
	std::string n = "";
	int m = 0, k = 0;
	cin >> n >> m >> k;
	std::vector< std::set<std::string> > sets(m + 1, std::set<std::string>());
	std::map<std::string, std::set<int>> numbers;
	while (k--) {
		std::string q = "";
		cin >> q;
		if (q == "ADD") {
			std::string num = "";
			cin >> num;
			makeLonger(num);
			int setId = 0;
			cin >> setId;
			sets[setId].insert(num);
			if(numbers.find(num) == numbers.end())
				numbers[num] = std::set<int>();
			numbers[num].insert(setId);
		} else if (q == "DELETE") {
			std::string num = "";
			cin >> num;
			makeLonger(num);
			int setId = 0;
			cin >> setId;
			sets[setId].erase(num);
			numbers[num].erase(setId);
		} else if (q == "CLEAR") {
			int setId = 0;
			cin >> setId;
			for (auto t : sets[setId])
				numbers[t].erase(setId);
			sets[setId].clear();
		} else if (q == "LISTSET") {
			int setId = 0;
			cin >> setId;
			if (sets[setId].empty()) {
				cout << "-1\n";
				continue;
			}
			for (auto t : sets[setId]) {
				reverse(t.begin(), t.end());
				while (t.size() > 1 && t.back() == '0')
					t.pop_back();
				reverse(t.begin(), t.end());
				cout << t << " ";
			}
			cout << "\n";
		} else if (q == "LISTSETSOF") {
			std::string num = "";
			cin >> num;
			makeLonger(num);
			if (numbers.find(num) == numbers.end() || numbers[num].empty()) {
				cout << "-1\n";
				continue;
			}
			for (auto t : numbers[num])
				cout << t << " ";
			cout << "\n";
		}
	}
}
