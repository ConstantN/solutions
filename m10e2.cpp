#include <iostream>
#include <vector>
#include <algorithm>

/*
Задача:
Требуется найти в связном графе остовное дерево минимального веса.

Воспользуйтесь алгоритмом Крускала.

Входные данные
Первая строка входного файла содержит два натуральных числа n и m —
количество вершин и ребер графа соответственно.
Следующие m строк содержат описание ребер по одному на строке.
Ребро номер i описывается тремя натуральными числами bi, ei и wi —
номера концов ребра и его вес соответственно (1≤bi,ei≤n, 0≤wi≤100000). n≤20000,m≤100000.

Граф является связным.

Выходные данные
Первая строка выходного файла должна содержать одно натуральное число —
вес минимального остовного дерева.

Решение:
Стандартная реализация.
*/

struct Edge {
    int begin;
    int end;
    int weight;
    bool operator<(const Edge& e) const {
        return weight < e.weight;
    }
};

class DSU{
public:
    DSU(int n): p(n), size(n, 1) {
        for (int i = 0; i < n; i++) {
            p[i] = i;
        }
    }
    bool unite(int a, int b){
        a = get_par(a), b = get_par(b);
        if (a == b) {
            return false;
        }
        if (size[a] < size[b]) {
            std::swap(a, b);
        }
        p[b] = a;
        size[a] += size[b];
        return true;
    }
private:
    int get_par(int v) {
        if (p[v] == v) {
            return v;
        }
        return p[v] = get_par(p[v]);
    }
    std::vector<int> p;
    std::vector<int> size;
};

int findMST(int n, int m, std::vector<Edge> edges) {
    std::sort(edges.begin(), edges.end());
    DSU dsu(n);
    int ans = 0;
    for (auto &e: edges) {
        if (dsu.unite(e.begin, e.end)) {
            ans += e.weight;
        }
    }
    return ans;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0, m = 0;
    std::cin >> n >> m;
    std::vector<Edge> edges(m);
    for (auto &e : edges) {
        std::cin >> e.begin >> e.end >> e.weight;
        e.begin--, e.end--;
    }
    std::cout << findMST(n, m, edges);
}
