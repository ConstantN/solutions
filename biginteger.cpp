#include <iostream>
#include <vector>
#include <string>

class BigInteger {
friend std::istream& operator>>(std::istream&, BigInteger&);
public:
	BigInteger(int);
	BigInteger();
	BigInteger& operator+=(const BigInteger&);
	BigInteger operator+() const;
	BigInteger& operator++();
	BigInteger operator++(int);
	BigInteger& operator-=(const BigInteger&);
	BigInteger operator-() const;
	BigInteger& operator--();
	BigInteger operator--(int);
	BigInteger& operator*=(const BigInteger&);
	BigInteger& operator/=(const BigInteger&);
	BigInteger& operator%=(const BigInteger&);
	bool operator<(const BigInteger&) const;
	bool operator>(const BigInteger&) const;
	bool operator==(const BigInteger&) const;
	bool operator!=(const BigInteger&) const;
	bool operator<=(const BigInteger&) const;
	bool operator>=(const BigInteger&) const;
	explicit operator int() const;
	explicit operator bool() const;
	std::string toString() const;
private:
	static const int BASE = 10;
	bool isPositive; //Zero is positive
	std::vector<short> digits;
	void normilize();
	void popBackZeros(); 
	void fixMinusZero();
	void reverseDigits();
	void addDigit(short);	
};

std::istream& operator>>(std::istream& in, BigInteger& num) {
	char c;
	bool firstSymbolDefined = false;
	num.isPositive = true;
	num.digits.clear();
	while(in.get(c)) {
		if (c == ' ' || c == '\n') {
			if (firstSymbolDefined) {
				break;
			} else {
				continue;
			}
		}
		firstSymbolDefined = true;
		if (c == '-') {
			num.isPositive = false;	
		} else if (c == '+') {
			num.isPositive = true;
		} else if (c >= '0' && c <= '9') {
			num.digits.push_back(c - '0');
		}
	}		
	num.reverseDigits();
	num.normilize();
	return in;
}

std::ostream& operator<<(std::ostream& out, const BigInteger& num) {
	out << num.toString();
	return out;
}

BigInteger::BigInteger(int value) {
	isPositive = (value >= 0);
	if (!isPositive)
		value = -value;
	if (!value) digits.push_back(0);
	while (value) {
		digits.push_back(value % BASE);
		value /= BASE;
	}
}

BigInteger::BigInteger() : BigInteger(0) {
}

BigInteger& BigInteger::operator+=(const BigInteger& num) {
	if (this == &num) {
		return *this += BigInteger(num);
	}
	if (num.isPositive == isPositive) {
		for (unsigned int i = 0; i < std::max(num.digits.size(), digits.size()); ++i) {
			short add = (i < num.digits.size() ? num.digits[i] : 0);
			if (digits.size() == i)
				digits.push_back(0);
			if (digits[i] + add >= BASE) {
				if (digits.size() == i + 1)
					digits.push_back(0);
				++digits[i + 1];
			} else if (i >= num.digits.size()) {
				digits[i] = (digits[i] + add) % BASE;
				break;
			}
			digits[i] = (digits[i] + add) % BASE;
		}
	} else {
		isPositive = !isPositive;
		*this -= num;
		isPositive = !isPositive;
	}
	normilize();
	return *this;
}

BigInteger operator+(const BigInteger& num1, const BigInteger& num2) {
	BigInteger copy = num1;
	return copy += num2;
}

BigInteger BigInteger::operator+() const {
	return *this;
}

BigInteger& BigInteger::operator++() {
	return (*this += 1);
}

BigInteger BigInteger::operator++(int) {
	BigInteger copy = *this;
	++(*this);
	return copy;
}

BigInteger& BigInteger::operator-=(const BigInteger& num) {
	if (this == &num) {
		return *this -= BigInteger(num);
	}
	if (num.isPositive == isPositive) {
		bool needChange = isPositive ^ (num <= *this); 
		const BigInteger& a = needChange ? num : *this;
		const BigInteger& b = needChange ? *this : num;
		bool need = false;
		for (unsigned int i = 0; i < a.digits.size(); ++i) {
			bool needNext = (a.digits[i] - need - (i < b.digits.size() ? b.digits[i] : 0) < 0);
			if(i == digits.size()) digits.push_back(0);
			digits[i] = (a.digits[i] - need - (i < b.digits.size() ? b.digits[i] : 0) + BASE) % BASE;
			need = needNext;
		}
		isPositive ^= needChange;
	} else {
		isPositive = !isPositive;
		*this += num;
		isPositive = !isPositive;
	}
	normilize();
	return *this;
}

BigInteger operator-(const BigInteger& num1, const BigInteger& num2) {
	BigInteger copy = num1;
	return copy -= num2;
}

BigInteger BigInteger::operator-() const {
	return BigInteger(0) -= *this;
}

BigInteger& BigInteger::operator--() {
	return (*this -= 1);
}

BigInteger BigInteger::operator--(int) {
	BigInteger copy = *this;
	--(*this);
	return copy;
}

BigInteger& BigInteger::operator*=(const BigInteger& num) {
	BigInteger ans;
	ans.digits.resize(digits.size() + num.digits.size());
	for(unsigned int i = 0; i < digits.size(); ++i) {
		int add = 0;
		for (unsigned int j = 0; j <= num.digits.size(); ++j) {
			short numDigit = (j == num.digits.size() ? 0 : num.digits[j]);
			short addNext = (ans.digits[i + j] + digits[i] * numDigit + add) / BASE;
			ans.digits[i + j] = (ans.digits[i + j] + digits[i] * numDigit + add) % BASE;
			add = addNext; 
		}
	}
	ans.isPositive = !(isPositive ^ num.isPositive);
	ans.normilize();
	return *this = ans;
}

BigInteger operator*(const BigInteger& num1, const BigInteger& num2) {
	BigInteger copy = num1;
	return copy *= num2;
}

BigInteger& BigInteger::operator/=(const BigInteger& num) {
	if(num == 0)
		return *this;
	BigInteger ans, val(digits.back());
	ans.isPositive = !(isPositive ^ num.isPositive);
	int ptr = digits.size();
	ptr -= 2;
	val.isPositive = num.isPositive;
	while (ptr >= 0 && (num.isPositive ? val < num : num < val)) {
		val.addDigit(digits[ptr]);
		--ptr;
	}
	val.isPositive = true;
	for (; ptr >= -1; --ptr) {
		int number = 0;
		while (val >= 0) {
			num.isPositive ? val -= num : val += num;
			++number;
		}
		num.isPositive ? val += num : val -= num;
		--number;
		
		ans.digits.push_back(number);
		if(ptr != -1)
			val.addDigit(digits[ptr]);
	}
	ans.reverseDigits();
	ans.normilize();
	return *this = ans;
}

BigInteger operator/(const BigInteger& num1, const BigInteger& num2) {
	BigInteger copy = num1;
	return copy /= num2;
} 

BigInteger& BigInteger::operator%=(const BigInteger& num) {
	if(num == 0) 
		return *this;
	BigInteger copy = (*this) / num;
	return *this -= (copy *= num);
}

BigInteger operator%(const BigInteger& num1, const BigInteger& num2) {
	BigInteger copy = num1;
	return copy %= num2;
}

bool BigInteger::operator<(const BigInteger& num) const {
	if (isPositive != num.isPositive)
		return num.isPositive;
	for (int j = std::max(static_cast<int>(digits.size()), static_cast<int>(num.digits.size())) - 1; j >= 0; --j) {
		unsigned int i = static_cast<unsigned int>(j);
		if ((i < digits.size() ? digits[i] : 0) < (i < num.digits.size() ? num.digits[i] : 0)) return isPositive;
		if ((i < digits.size() ? digits[i] : 0) > (i < num.digits.size() ? num.digits[i] : 0)) return !isPositive;
	}
	return false;
}

bool BigInteger::operator>(const BigInteger& num) const {
	return num < *this;
}

bool BigInteger::operator==(const BigInteger& num) const {
	return !(num < *this || *this < num);
}

bool BigInteger::operator!=(const BigInteger& num) const {
	return num < *this || *this < num;	
}

bool BigInteger::operator<=(const BigInteger& num) const {
	return !(num < *this);
}

bool BigInteger::operator>=(const BigInteger& num) const {
	return !(*this < num);
}

std::string BigInteger::toString() const {
	std::string num = "";
	if (!isPositive) num += '-';
	for (int i = digits.size() - 1; i >= 0; --i) {
		num += ('0' + digits[i]);
	}
	return num;
}

BigInteger::operator int() const {
	int value = 0;
	for (int i = digits.size() - 1; i >= 0; --i)
		value = value * 10 + digits[i];
	if(!isPositive)
		value = -value;
	return value;	
}

BigInteger::operator bool() const {
	return int(*this);
}

void BigInteger::normilize() {
	popBackZeros();
	fixMinusZero();
}

void BigInteger::popBackZeros() {
	while(digits.size() > 1 && digits.back() == 0) {
		digits.pop_back();
	}
}

void BigInteger::fixMinusZero() {
	isPositive = !isPositive;
	if (*this == BigInteger(0)) 
		isPositive = !isPositive;
	isPositive = !isPositive;
}

void BigInteger::reverseDigits() {
	int size = digits.size();
	for (int i = 0; i < size / 2; ++i) {
		std::swap(digits[i], digits[size-1-i]);
	}
}

void BigInteger::addDigit(short digit) {
		reverseDigits();
		digits.push_back(digit);
		reverseDigits();
}

class Rational{
private:
	BigInteger numerator, denominator;
	void normilize();
	static BigInteger gcd(const BigInteger&, const BigInteger&);
	static void reverse(std::string&);
public:
	Rational();
	Rational(const BigInteger&);
	Rational(int);
	Rational& operator+=(const Rational&);
	Rational operator+() const;
	Rational& operator-=(const Rational&);
	Rational operator-() const;
	Rational& operator*=(const Rational&);
	Rational& operator/=(const Rational&);
	bool operator<(const Rational&) const;
	bool operator>(const Rational&) const;
	bool operator==(const Rational&) const;
	bool operator!=(const Rational&) const;
	bool operator<=(const Rational&) const;
	bool operator>=(const Rational&) const;
	explicit operator double() const;
	std::string asDecimal(size_t) const;
	std::string toString() const;
};

BigInteger Rational::gcd(const BigInteger& a, const BigInteger& b) {
	if (b == 0)
		return a;
	return gcd(b, a % b); 
}

void Rational::normilize() {
	BigInteger _gcd = gcd(numerator, denominator);
	numerator /= _gcd;
	denominator /= _gcd;
	if (denominator < 0) {
		numerator *= -1;
		denominator *= -1;
	}
}

void Rational::reverse(std::string& s) {
	for(size_t i = 0; i < s.size()/2; ++i) {
		std::swap(s[i], s[s.size() - 1 - i]);
	}
}

Rational::Rational() : Rational(0) {
}

Rational::Rational(const BigInteger& val) {
	numerator = val;
	denominator = 1;
}

Rational::Rational(int val) {
	numerator = val;
	denominator = 1;
}

Rational& Rational::operator+=(const Rational& frac) {
	if (this == &frac)
		return *this += Rational(frac);
	numerator *= frac.denominator;
	numerator += denominator * frac.numerator;
	denominator *= frac.denominator;
	normilize();
	return *this;
}

Rational operator+(const Rational& frac1, const Rational& frac2) {
	Rational copy = frac1;
	return copy += frac2;
}

Rational Rational::operator+() const {
	return *this;
}

Rational& Rational::operator-=(const Rational& frac) {
	if (this == &frac)
		return *this -= Rational(frac);
	numerator *= frac.denominator;
	numerator -= denominator * frac.numerator;
	denominator *= frac.denominator;
	normilize();
	return *this;
}

Rational operator-(const Rational& frac1, const Rational& frac2) {
	Rational copy = frac1;
	return copy -= frac2;
}

Rational Rational::operator-() const {
	return Rational(0) -= *this;
}

Rational& Rational::operator*=(const Rational& frac) {
	if (this == &frac)
		return *this *= Rational(frac);
	numerator *= frac.numerator;
	denominator *= frac.denominator;
	normilize();
	return *this;
} 

Rational operator*(const Rational& frac1, const Rational& frac2) {
	Rational copy = frac1;
	return copy *= frac2;
}

Rational& Rational::operator/=(const Rational& frac) {
	if (frac.numerator == 0)
		return *this;
	if (this == &frac)
		return *this /= Rational(frac);
	numerator *= frac.denominator;
	denominator *= frac.numerator;
	normilize();
	return *this;
}

Rational operator/(const Rational& frac1, const Rational& frac2) {
	Rational copy = frac1;
	return copy /= frac2;
}

bool Rational::operator<(const Rational& frac) const {
	return numerator * frac.denominator < frac.numerator * denominator;
}

bool Rational::operator>(const Rational& frac) const {
	return frac < *this;
}

bool Rational::operator==(const Rational& frac) const {
	return !(frac < *this || *this < frac);
}

bool Rational::operator!=(const Rational& frac) const {
	return frac < *this || *this < frac;
}

bool Rational::operator<=(const Rational& frac) const {
	return !(frac < *this);
}

bool Rational::operator>=(const Rational& frac) const {
	return !(*this < frac);
}

Rational::operator double() const {
    return std::stod(asDecimal(200));
}

std::string Rational::asDecimal(size_t precision = 0u) const {
	BigInteger ten = 1;
	for (size_t i = 0; i < precision; ++i) {
		ten *= 10;
	}
	BigInteger val = (numerator < 0 ? -1 : 1) * numerator * ten / denominator;
	std::string integer = (val / ten).toString();
	std::string frac = (val % ten).toString();
	reverse(frac);
	while(frac.size() < precision) frac += '0';
	frac += '.';
	reverse(frac);
	if(precision == 0u) frac = "";
	integer = (numerator < 0 ? "-" : "") + integer;
	return integer + frac;
}

std::string Rational::toString() const {
	std::string num = numerator.toString();
	if (denominator != 1) {
		num += "/" + denominator.toString();
	}
	return num;
}
