#include <iostream>
#include <vector>

/*
Условие:
Дано n нестрого возрастающих массивов, a[i] - Итый из них
m нестрого убывающих массивов, b[i] - Итый из них
У всех них одинаковая длина l.
Затем идёт q запросов вида indexA,indexB - ответ на каждый из них: (минимальный по всем k) max(a[indexA][k],b[indexB][k])

Решение:
Представим возрастающие массивы в виде ломанных на XoY, монотонно возрастающих
Убывающие массивы - в виде ломанных на XoY, монотонно убывающих
Тогда когда приходит запрос i,j - представим как две эти прямые пересекаются
поскольку нам нужно найти (минимальный по всем k) max(a[indexA][k],b[indexB][k]), то
искомая величина будет находиться около точки пересечения этих прямых.
Слева от точки пересечения a[indexA][k] < b[indexB][k], справа от этой точки a[indexA][k] >= b[indexB][k]
*/

int answerQuery(std::vector< std::vector<int> > &a, int sizeA, std::vector< std::vector<int> > &b, int sizeB, int length, int indexA, int indexB){
	indexA--;
	indexB--;
	int L = -1; // L поддерживаем на индексе, где a[indexA]<b[indexB]
	int R = length; // R поддерживаем на индексе, где a[indexA]>=b[indexB]
	// Для простоты будем считать что в фиктивных индексах -1 и length выполняются соответствующие отношения
	while (L + 1 < R) {
		int M = (L + R) / 2;
		if (a[indexA][M] < b[indexB][M]) {
			L = M;
		} else {
			R = M;
		}
	}
	// После данного бинпоиска L и R - соседние числа
	// Исходя из определения L и R - это индексы около точки пересечния - слева и справа
	int bestVal = 1e5;
	int bestInd = -1;
	for (int check = L; check <= R; ++check) { // Найдём наилучший из двух индексов 
		if (check >= 0 && check < length) { // Проверяем, что check - не фиктивный индекс 
			if (std::max(a[indexA][check], b[indexB][check]) < bestVal) { 
				bestVal = std::max(a[indexA][check], b[indexB][check]); // Обновляем наилучшие значения
				bestInd = check;
			}
		}	
	}
	return bestInd + 1;
}

int main(){	
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	int indexA = 0, indexB = 0;
	int sizeA = 0, sizeB = 0;
	int length = 0;
	std::cin >> sizeA >> sizeB >> length;
	std::vector<std::vector<int>> a(sizeA, std::vector<int>(length)), b(sizeB, std::vector<int>(length));
	for(int i = 0; i < sizeA; ++i){
		for(int j = 0; j < length; ++j){
			std::cin >> a[i][j];
		}
	}
	for(int i = 0; i < sizeB; ++i){
		for(int j = 0; j < length; ++j){
			std::cin >> b[i][j];
		}	
	}

	int queryNumber = 0;
	std::cin >> queryNumber;
	while (queryNumber--) {
		std::cin >> indexA >> indexB;
		std::cout << answerQuery(a, sizeA, b, sizeB, length, indexA, indexB) << "\n";
	}
	return 0;
}
