#include <iostream>
#include <vector>

/*
Условия:
Компания BrokenTiles планирует заняться выкладыванием во дворах у состоятельных клиентов узор из черных и белых плиток, каждая из которых имеет размер 1×1 метр. Известно, что дворы всех состоятельных людей имеют наиболее модную на сегодня форму прямоугольника M×N метров.

Однако при составлении финансового плана у директора этой организации появилось целых две серьезных проблемы: во первых, каждый новый клиент очевидно захочет, чтобы узор, выложенный у него во дворе, отличался от узоров всех остальных клиентов этой фирмы, а во вторых, этот узор должен быть симпатичным.

Как показало исследование, узор является симпатичным, если в нем нигде не встречается квадрата 2×2 метра, полностью покрытого плитками одного цвета.

Для составления финансового плана директору необходимо узнать, сколько клиентов он сможет обслужить, прежде чем симпатичные узоры данного размера закончатся. Помогите ему!

Входные данные
На первой строке входного файла находятся два положительных целых числа, разделенные пробелом — M и N (1⩽M×N⩽30).

Выходные данные
Выведите в выходной файл единственное число — количество различных симпатичных узоров, которые можно выложить во дворе размера M×N. Узоры, получающиеся друг из друга сдвигом, поворотом или отражением считаются различными.

Решение:
dp[i][mask] - количество узоров ширины i, где последний ряд: mask.
*/

bool hasNeigbouringOne(int mask, int len) {
    bool prev = false;
    for (int i = 0; i < len; i++) {
        if (mask & (1 << i)) {
            if (prev) {
                return true;
            }
            prev = true;
        } else { 
            prev = false;
        }
    }
    return false;
}

bool isCompatibleMasks(int mask1, int mask2, int len) {
    int white = mask1 & mask2;
    int black = (~mask1) & (~mask2);
    return (!hasNeigbouringOne(white, len)) & (!hasNeigbouringOne(black, len));
}

long long countSequences(int n, int m) {
    if (n < m) {
        std::swap(n, m);
    }
    const int masks = (1 << m);
    std::vector<std::vector<long long>> dp(n, std::vector<long long>(masks));
    for (int i = 0; i < masks; i++) {
        dp[0][i] = 1;
    }
    for (int i = 1; i < n; i++) {
        for (int mask = 0; mask < masks; mask++) {
            for (int prev_mask = 0; prev_mask < masks; prev_mask++) {
                if (isCompatibleMasks(mask, prev_mask, m)) {
                    dp[i][mask] += dp[i - 1][prev_mask];
                }
            }
        }
    }
    long long ret = 0;
    for (int i = 0; i < masks; i++) {
        ret += dp.back()[i];
    }
    return ret;
}

int main() {
    int n = 0, m = 0;
    std::cin >> n >> m;
    std::cout << countSequences(n, m);
}
