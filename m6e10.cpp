#include <iostream>
#include <vector>

/*
Найдите максимальный вес золота, который можно унести в рюкзаке вместительностью S, если есть N золотых слитков с заданными весами.

Входные данные
В первой строке входного файла запианы два числа — S и N (1⩽S⩽10000, 1⩽N⩽300). Далее следует N неотрицательных целых чисел, не превосходящих 100 000 — веса слитков.

Выходные данные
Выведите искомый максимальный вес.

Решение: стандартный рюкзак, где f[i] = (можно ли собрать вес i)
*/

int maxWeight(const std::vector<int>& w, int W) {
	std::vector<int> f(W + 1, 0);
	f[0] = 1;
	for (int i = 0; i < w.size(); i++) {
		for (int j = W; j >= 0; j--) {
			if (f[j] && j + w[i] <= W) {
				f[j + w[i]] = 1;
			}
		}
	}
	int id = 0;
	for (int i = W; i >= 0; i--) {
		if (f[i]) {
			id = i;
			break;
		}
	}
	return id;
}

int main() {
	int n = 0, W = 0;
	std::cin >> W >> n;	
	std::vector<int> w(n);
	for (auto &t:w) {
		std::cin >> t;
	}
	std::cout << maxWeight(w, W);
}
