#include <iostream>

struct Node{
	int val;
	Node* prev;
};

struct Stack{
	Node* last;
	int size;
};

void initStack(Stack* stack){
	stack->last = 0;
	stack->size = 0;
}

int size(Stack* stack){
	return stack->size;
}

int lastElement(Stack* stack){
	return stack->last->val;
}

bool isEmpty(Stack* stack){
	return (stack->size == 0);
}

int popElement(Stack* stack){
	Node* last = stack->last;
	int lastVal = last->val;
	stack->last = last->prev;
	stack->size--;
	free(last);
	return lastVal;
}

void pushElement(Stack* stack, int val){
	Node* node = (Node*)malloc(sizeof(Node));
	node->val = val;
	node->prev = stack->last;
	stack->last = node;
	stack->size++;
}

void deleteStack(Stack* stack){
	Node* lst = stack->last;
	while(!isEmpty(stack)){
		popElement(stack);
	}
}

int main(){
	Stack st;
	initStack(&st);

	std::string s;
	while(std::cin>>s){
		if(s=="push"){
			int val;
			std::cin>>val;
			pushElement(&st,val);	
			std::cout<<"ok\n";
		}else if(s=="pop"){
			if(isEmpty(&st)) std::cout<<"error\n";
			else std::cout<<popElement(&st)<<"\n";
		}else if(s=="back"){
			if(isEmpty(&st)) std::cout<<"error\n";
			else std::cout<<lastElement(&st)<<"\n";
		}else if(s=="size"){
			std::cout<<size(&st)<<"\n";
		}else if(s=="clear"){
			deleteStack(&st);
			std::cout<<"ok\n";
		}else if(s=="exit"){
			std::cout<<"bye\n";
			return 0;
		}
	}
	return 0;
}
