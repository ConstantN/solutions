#include <iostream>
#include <vector>
#include <algorithm>

/*
Вам требуется написать программу, которая по заданной последовательности находит максимальную невозрастающую её подпоследовательность (т.е такую последовательность чисел ai1,ai2,…,aik (i1<i2<…<ik), что ai1≥ai2≥…≥aik и не существует последовательности с теми же свойствами длиной k+1).

Входные данные
В первой строке задано число n — количество элементов последовательности (1≤n≤239017). В последующих строках идут сами числа последовательности ai, отделенные друг от друга произвольным количеством пробелов и переводов строки (все числа не превосходят по модулю 231−2).

Выходные данные
Вам необходимо выдать в первой строке выходного файла число k — длину максимальной невозрастающей подпоследовательности. В последующих строках должны быть выведены (по одному числу в каждой строке) все номера элементов исходной последовательности ij, образующих искомую подпоследовательность. Номера выводятся в порядке возрастания. Если оптимальных решений несколько, разрешается выводить любое.

--

Идея - научимся находить неубывающую последовательность, а затем сведём исходную задачу к этой.
Как её искать?
Скажу лишь что в каждый момент цикла, dp[i] = минимальное число, на которое может заканчиваться НВП размера i.
Ещё из интересного, в каждый момент dp - "возрастающий" массив
А когда мы хотим обновить dp с помощью очередного числа А,
    то найдём первое число, бОльшее нашего и заменим его на А
    (потому что можем составить новую НВП длины i, добавив к dp[i - 1] число А в конец и улучшить dp[i])
На этом всё.
*/

void findMIS(const std::vector<int>& v, std::vector<int>& ans) {
	int n = v.size(), inf = INT32_MAX;
	std::vector<std::pair<int, int>> dp(n + 1, {inf, -1});
	std::vector<int> p(n + 1, -1);
	dp[0].first = -inf;
	for (int i = 0; i < n; i++) {
		int id = upper_bound(dp.begin(), dp.end(), std::make_pair(v[i], inf)) - dp.begin();
		dp[id] = {v[i], i};
		p[i] = dp[id - 1].second;
	}
	int i;
	for (i = n; i >= 0; i--) {
		if (dp[i].first != inf) {
			break;
        }
    }
	i = dp[i].second;
	while (i != -1) {
		ans.push_back(i);
		i = p[i];
	}
	std::reverse(ans.begin(), ans.end());
}

void findMDS(std::vector<int> v, std::vector<int>& ans) {
	for (auto &t : v) {
		t = -t;
    }
	findMIS(v, ans);
}

int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0;
	std::cin >> n;
	std::vector<int> v(n);
	for (auto &t : v) {
		std::cin >> t;
    }
	std::vector<int> ans;
    findMDS(v, ans);
	std::cout << ans.size() << std::endl;
	for (auto t : ans) {
		std::cout << t + 1 << " ";
    }
}
