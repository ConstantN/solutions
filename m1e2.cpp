#include <iostream>
#include <string>
#include <stack>

/*
Дана строка, состоящая из круглых, квадратных и фигурных скобок. Проверить - является ли она ПСП.
*/

char returnPair(char c){
	if(c==']') return '[';
	if(c=='}') return '{';
	if(c==')') return '(';
	return (char)0;
}

bool isBBS(std::string seq){
	std::stack<char> st;
	bool ok = true;
	for(int i = 0; i < seq.length(); i++){
		if(seq[i]=='(' || seq[i]=='[' || seq[i]=='{'){ //Добавляю скобку в стэк
			st.push(seq[i]);
		}else{
			if(!st.empty() && st.top()==returnPair(seq[i])) { //Удаляю, если парная
				st.pop();
			}else{
				ok = false; //Если хоть раз не парная - ответ No
				break;
			}
		}
	}
	ok &= st.empty(); //Также проверям, что стэк пустой
	return ok;
}

int main(){
	std::string input;
	std::getline(std::cin,input);
	std::cout<<(isBBS(input)?"yes":"no");
	return 0;
}
