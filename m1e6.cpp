#include <iostream>
 
using std::cin;
using std::cout;
 
//Задача - найти количество инверсий на массиве.

//Функция merge решает задачу: слить два отсортированных массива и посчиать количество инверсий из разных половинок
long long merge(int* v, int* tmp, int l, int m, int r) {
	int ptrRight = m + 1;
	int ptr = l;
	int qRight = 0; //qRight - поддерживаем, сколько из right половинки положили 
	long long ans = 0;
	for (int ptrLeft = l; ptrLeft <= m; ++ptrLeft) { // Итерируемся по массиву left
		while (ptrRight <= r && v[ptrRight] < v[ptrLeft]) { // Пока есть что взять из массива right и это меньше, чем то, что мы хотим положить из массива left - кладём из right
			tmp[ptr] = v[ptrRight];
			++ptr;
			++ptrRight;
			++qRight;	
		}
		tmp[ptr] = v[ptrLeft]; // Кладём наконец из left
		++ptr;
		ans += qRight; // Прибавляем к ответу количество инверсий, в которых участвует элемент v[ptrLeft]
		//Доказательство: пусть мы положили уже qRight из правой половинки, значит все они меньше, чем
		//v[ptrLeft], т.к. сортируем по возрастанию. Однако поскольку они из right половинки - 
		//то они стояли справа от элемента v[ptrLeft] в изначальном массиве, значит qRight и есть
		//количество инверсий с участием v[ptrLeft] и элемент из правой половинке.
	}
	for (int i = l; i < ptr; ++i) { // Переприсваиваем, т.к. клали всё это время в массив tmp
		v[i] = tmp[i];
	}	
	return ans;
}

// Функция mergeSort решает задачу: отсортировать отрезок [l,r] массива v по возрастанию
// и вернуть количество инверсий на нём.
// Принимает в качестве параметра также указатель на вспомогательный массив tmp
long long mergeSort(int* v, int* tmp, int l, int r) { 
	if(l == r) return 0; // База: на отрезке массива длинной в 1 нет инверсий и он отсортирован
	int m = (l + r) / 2;
	long long ans = 0;
	ans += mergeSort(v, tmp, l, m); // Решаем задачу на половинке left
	ans += mergeSort(v, tmp, m + 1, r); // Решаем задачу на половинке right
	ans += merge(v, tmp, l, m, r);
	return ans;
}
 
int main(){
	int n = 0;
	cin >> n;
	int* tmp = new int[n];
	int* v = new int[n];
	for (int i = 0; i < n; ++i) {
		cin >> v[i];
	}
	cout << mergeSort(v, tmp, 0, n-1);
	delete[] tmp;
	delete[] v;
}
