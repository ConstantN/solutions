#include <vector>
#include <iostream>
#include <cassert>
#include <deque>
#include <memory>

template<typename T>
class Deque{
public:
    Deque();
    Deque(const Deque<T>&);
    Deque(const std::deque<T>&);
    explicit Deque(const size_t);
    Deque(const size_t, const T&);
    Deque<T>& operator=(const Deque<T>&);
    Deque<T>& operator=(const std::deque<T>&);
    ~Deque();
    size_t size() const;
    T& operator[](const size_t);
    const T& operator[](const size_t) const;
    T& at(const int);
    const T& at(const int) const;
    void push_back(const T&);
    void push_front(const T&);
    void pop_back();
    void pop_front();

    template<bool IsConst>
    struct common_iterator;
    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator cbegin() const;
    const_iterator end() const;
    const_iterator cend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crend() const;

    void insert(iterator, const T&);
    void erase(iterator);
private:
    int balance;
    size_t capacity;
    size_t begin_index;
    size_t end_index;
    T* data;
    const size_t EXPANSION_COEFFICIENT = 2;
    const size_t CONST_ADDITION = 5;
    void swap(Deque<T>&);
    void reallocate();
};

template<typename T>
template<bool IsConst>
struct Deque<T>::common_iterator {
public:
    using difference_type = std::ptrdiff_t;
    using value_type = typename std::conditional<IsConst, const T, T>::type;
    using pointer = typename std::conditional<IsConst, const T*, T*>::type;
    using reference = typename std::conditional<IsConst, const T&, T&>::type;
    using iterator_category = std::random_access_iterator_tag;
    using dequeType = typename std::conditional<IsConst, const Deque<T>*, Deque<T>*>::type;
    common_iterator(dequeType deque, const int index): deque(deque), index(index), start_balance(deque->balance) {
    }

    reference operator*() const {
        return (*deque)[get_index()];
    }
    pointer operator->() const {
        return &((*deque)[get_index()]);
    }
    common_iterator<IsConst>& operator++() {
        ++index;
        return *this;
    }
    common_iterator<IsConst>& operator--() {
        --index;
        return *this;
    }
    common_iterator<IsConst>& operator+=(const int i) {
        index += i;
        return *this;
    }
    common_iterator<IsConst>& operator-=(const int i) {
        index -= i;
        return *this;
    }
    common_iterator<IsConst> operator+(const int i) const {
        common_iterator<IsConst> copy = *this;
        copy += i;
        return copy;
    }
    common_iterator<IsConst> operator-(const int i) const {
        common_iterator<IsConst> copy = *this;
        copy -= i;
        return copy;
    }
    int operator-(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return get_index() - iter.get_index();
    }
    bool operator<(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() < iter.get_index());
    }
    bool operator<=(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() <= iter.get_index());
    }
    bool operator>(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() > iter.get_index());
    }
    bool operator>=(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() >= iter.get_index());
    }
    bool operator==(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() == iter.get_index());
    }
    bool operator!=(const typename Deque<T>::template common_iterator<IsConst> iter) const {
        return (get_index() != iter.get_index());
    }
    operator common_iterator<true>() const {
        return common_iterator<true>(index, start_balance, deque);
    }
    friend common_iterator<true>;
    friend common_iterator<false>;
private:
    dequeType deque;
    int index;
    int start_balance;
    int get_index() const {
        return index + deque->balance - start_balance;
    }
    common_iterator<IsConst>(const int index, const int start_balance, const Deque<T>* deque): index(index), start_balance(start_balance), deque(deque) {}
};


template<typename T, bool IsConst>
typename Deque<T>::template common_iterator<IsConst> operator+(const int i, const typename Deque<T>::template common_iterator<IsConst>& iter) {
    return (iter + i);
}

template<typename T>
Deque<T>::Deque() {
    balance = 0;
    capacity = 1;
    begin_index = end_index = 0;
    data = reinterpret_cast<T*>(new int8_t[capacity * sizeof(T)]);
}

template<typename T>
Deque<T>::Deque(const Deque<T>& deque) {
    balance = deque.balance;
    capacity = deque.capacity;
    begin_index = deque.begin_index;
    end_index = deque.end_index;
    data = reinterpret_cast<T*>(new int8_t[capacity * sizeof(T)]);
    try {
        std::uninitialized_copy(deque.data + begin_index, deque.data + end_index, data + begin_index);
    } catch (...) {
        delete[] reinterpret_cast<int8_t*>(data);
        throw;
    }
}

template<typename T>
Deque<T>::Deque(const std::deque<T>& deque) {
    balance = 0;
    capacity = deque.size();
    begin_index = 0;
    end_index = capacity;
    data = reinterpret_cast<T*>(new int8_t[capacity * sizeof(T)]);
    for (size_t i = 0; i < deque.size(); ++i) {
        try {
            new(data + begin_index + i) T(deque[i]);
        } catch (...) {
            for (size_t j = 0; j < i; ++j) {
                (data + begin_index + j)->~T();
            }
            delete[] reinterpret_cast<int8_t*>(data);
            throw;
        }
    }
}

template<typename T>
Deque<T>::Deque(const size_t size) {
    balance = 0;
    capacity = size;
    begin_index = 0;
    end_index = size;
    data = reinterpret_cast<T*>(new int8_t[capacity * sizeof(T)]);
    for (size_t i = 0; i < size; ++i) {
        try {
            new(data + begin_index + i) T();
        } catch (...) {
            for (size_t j = 0; j < i; ++j) {
                (data + begin_index + j)->~T();
            }
            delete[] reinterpret_cast<int8_t*>(data);
            throw;
        }
    }
}

template<typename T>
Deque<T>::Deque(const size_t size, const T& value) {
    balance = 0;
    capacity = size;
    begin_index = 0;
    end_index = size;
    data = reinterpret_cast<T*>(new int8_t[capacity * sizeof(T)]);
    for (size_t i = 0; i < size; ++i) {
        try {
            new(data + begin_index + i) T(value);
        } catch (...) {
            for (size_t j = 0; j < i; ++j) {
                (data + begin_index + j)->~T();
            }
            delete[] reinterpret_cast<int8_t*>(data);
            throw;
        }
    }
}

template<typename T>
Deque<T>& Deque<T>::operator=(const Deque<T>& deque) {
    Deque<T> copy(deque);
    swap(copy);
    return *this;
}

template<typename T>
Deque<T>& Deque<T>::operator=(const std::deque<T>& deque) {
    Deque<T> copy(deque);
    swap(copy);
    return *this;
}

template<typename T>
Deque<T>::~Deque() {
    for (size_t i = begin_index; i < end_index; ++i) {
        (data + i)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(data);
}

template<typename T>
size_t Deque<T>::size() const {
    return end_index - begin_index;
}

template<typename T>
T& Deque<T>::operator[](const size_t index) {
    return data[begin_index + index];
}

template<typename T>
const T& Deque<T>::operator[](const size_t index) const {
    return data[begin_index + index];
}

template<typename T>
T& Deque<T>::at(const int index) {
    if (begin_index + index >= end_index || index < 0) {
        throw(std::out_of_range("out_of_range"));
    }
    return data[begin_index + index];
}

template<typename T>
const T& Deque<T>::at(const int index) const {
    if (begin_index + index >= end_index || index < 0) {
        throw(std::out_of_range("out_of_range"));
    }
    return data[begin_index + index];
}

template<typename T>
void Deque<T>::push_back(const T& value) {
    try {
        reallocate();
        new(data + end_index) T(value);
    } catch (...) {
        throw;
    }
    end_index++;
}

template<typename T>
void Deque<T>::push_front(const T& value) {
    try {
        reallocate();
        new(data + begin_index - 1) T(value);
    } catch (...) {
        throw;
    }
    begin_index--;
    balance++;
}

template<typename T>
void Deque<T>::pop_back() { 
    (data + end_index - 1)->~T();
    --end_index;
}

template<typename T>
void Deque<T>::pop_front() {
    (data + begin_index)->~T();
    begin_index++;
    balance--;
}

template<typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return Deque::iterator(this, 0);
}

template<typename T>
typename Deque<T>::iterator Deque<T>::end() {
    return Deque::iterator(this, size());
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return Deque::const_iterator(this, 0);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
    return Deque::const_iterator(this, 0);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    return Deque::const_iterator(this, size());
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {
    return Deque::const_iterator(this, size());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return std::make_reverse_iterator(end());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    return std::make_reverse_iterator(begin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    return std::make_reverse_iterator(cend());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
    return std::make_reverse_iterator(cend());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const {
    return std::make_reverse_iterator(cbegin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    return std::make_reverse_iterator(cbegin());
}

template<typename T>
void Deque<T>::insert(Deque<T>::iterator iter, const T& value) {
    try {
        push_back(value);
    } catch (...) {
        throw;
    }
    const size_t ind = begin_index + (iter - begin());
    for (size_t i = end_index - 2; i >= ind; --i) {
        std::swap(data[i + 1], data[i]);
    }
}

template<typename T>
void Deque<T>::erase(Deque<T>::iterator iter) {
    for (size_t i = begin_index + (iter - begin()); i < end_index - 1; ++i) {
        std::swap(data[i], data[i + 1]);
    }
    pop_back();
}

template<typename T>
void Deque<T>::swap(Deque<T>& deque) {
    std::swap(balance, deque.balance);
    std::swap(capacity, deque.capacity);
    std::swap(begin_index, deque.begin_index);
    std::swap(end_index, deque.end_index);
    std::swap(data, deque.data);
}

template<typename T>
void Deque<T>::reallocate() {
    if (begin_index == 0 || end_index == capacity) {
        const size_t add_capacity = capacity * (EXPANSION_COEFFICIENT - 1) + CONST_ADDITION;
        T* new_data = reinterpret_cast<T*>(new int8_t[(capacity + add_capacity) * sizeof(T)]);
        
        try {
            std::uninitialized_copy(data + begin_index, data + end_index, new_data + add_capacity/2 + begin_index);
        } catch (...) {
            delete[] reinterpret_cast<int8_t*>(new_data);
            throw;
        }
        for (size_t i = begin_index; i < end_index; ++i) {
            (data + i)->~T();
        }
        std::swap(data, new_data);
        delete[] reinterpret_cast<int8_t*>(new_data);
        begin_index += add_capacity / 2;
        end_index += add_capacity / 2;
        capacity += add_capacity;
    }
}
