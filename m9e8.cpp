#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>
#include <limits>

/*
Задача:
Дано две перестановки чисел от 1 до n.

Определите минимальное количество операций разворота на отрезке
(изменения порядка нескольких подряд идущих чисел на противоположный)
первой перестановки, переводящих ее во вторую.

Входные данные
Первая строка содержит размер перестановки n (1⩽n⩽10).

Следующие две строки задают элементы перестановок.

Выходные данные
Выведите неотрицательное целое число — ответ на вопрос задачи.

Гарантируется, что хотя бы одна последовательность разворотов,
переводящих первую перестановку во вторую, существует.

Утверждение: 
Расстояние между любыми двумя <= 9.

Решение:
Запустим бфс с двух сторон глубиной 4.
*/

class Graph {
public:
    explicit Graph(int length): n(length) {
        precalc();
    }
    int getId(const std::string& s) const {
        int number = 0;
        int was = 0;
        for (int i = 0; i < n; i++) {
            int digit = (s[i] - '0');
            number += (digit - cnt_one[was & ((1 << digit) - 1)]) * fact[n - i - 1];
            was |= (1 << digit);
        }
        return number;
    }
    std::vector<int> getNeighbours(int id) const {
        std::string s = nodes[id];
        std::vector<int> ret;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                for (int q = 0; q < (j + 1 - i) / 2; q++) {
                    std::swap(s[i + q], s[j - q]);
                }
                ret.push_back(getId(s));
                for (int q = 0; q < (j + 1 - i) / 2; q++) {
                    std::swap(s[i + q], s[j - q]);
                }
            }
        }
        return ret;
    }
    int size() const {
        return fact[n];
    }
private:
    int n;
    std::vector<std::string> nodes;
    std::vector<int> fact;
    std::vector<int> cnt_one;
    void precalc() {
        fact.resize(n + 1);
        fact[0] = 1;
        for (int i = 1; i <= n; i++) {
            fact[i] = fact[i - 1] * i;
        }
        nodes.resize(size(), "");
        for (int i = 0; i < n; i++) {
            nodes[0].push_back('0' + i);
        }
        for (int i = 1; i < size(); i++) {
            nodes[i] = nodes[i - 1];
            std::next_permutation(nodes[i].begin(), nodes[i].end());
        }
        cnt_one.resize(1 << n);
        for (int i = 0; i < (1 << n); i++) {
            for (int j = 0; j < n; j++) {
                cnt_one[i] += ((i & (1 << j)) != 0);
            }
        }
    }
};

int findDistance(const Graph& graph, const std::string& from, const std::string& to) {
    std::vector<int> dest = {graph.getId(from), graph.getId(to)};
    std::vector<std::vector<int>> dist(2, std::vector<int>(graph.size(), std::numeric_limits<int>::max() / 2));
    const int bfsMaxDepth = 4;
    for (int i = 0; i < 2; i++) {
        std::queue<int> q;
        q.push(dest[i]);
        dist[i][dest[i]] = 0;
        while(!q.empty()) {
            int id = q.front();
            q.pop();
            if (dist[i][id] == bfsMaxDepth) {
                break;
            }
            for (auto id_fr: graph.getNeighbours(id)) {
                if (dist[i][id_fr] - 1 > dist[i][id]) {
                    dist[i][id_fr] = dist[i][id] + 1;
                    q.push(id_fr);
                }
            }
        }
    }
    int sum = std::numeric_limits<int>::max();
    for (int i = 0; i < graph.size(); i++) {
        sum = std::min(sum , dist[0][i] + dist[1][i]);
    }
    return (sum >= 10 ? 9 : sum);
}

main() { 
    int n = 0;
    std::cin >> n;
    std::vector<std::string> dest(2, "");
    Graph g(n);
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < n; j++) {
            int c = 0;
            std::cin >> c;
            dest[i].push_back('0' + (c - 1));
        }
    } 
    std::cout << findDistance(g, dest[0], dest[1]);
}
