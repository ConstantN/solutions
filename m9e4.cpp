#include <iostream>
#include <vector>
#include <tuple>
#include <algorithm>
#include <limits>

/*
Задача:
Дан взвешенный ориентированный граф и вершина s в нем.
Требуется для каждой вершины u найти длину кратчайшего пути из s в u.

Входные данные
Первая строка входного файла содержит n, m и s — количество вершин,
ребер и номер выделенной вершины соответственно (2≤n≤2000, 1≤m≤6000).

Следующие m строк содержат описание ребер. Каждое ребро задается стартовой вершиной,
конечной вершиной и весом ребра. Вес каждого ребра — целое число, не превосходящее 1015 по модулю
В графе могут быть кратные ребра и петли.

Выходные данные
Выведите n строк — для каждой вершины u выведите длину кратчайшего пути из s в u,
'*' если не существует путь из s в u и '-' если не существует кратчайший путь из s в u.

Решение:
Запустим Форда-Беллмана, а затем ещё одну итерацию.
Пометим все вершины, в которых изменился min_distance. Из них достижимы какие-то вершины.
Пометим их также. Для них дистанс = -inf.
Остальные вообще достигнутые из s -> distance = найденный ФБ мин_дистанс. (ans.flag = '-')
Все недостигнутые с помомщью ФБ - ans.flag = '*';
*/

enum PathState {
    PS_DoNotExists,
    PS_IsInfinite,
    PS_IsNormal
};

struct Distance{
    PathState state;
    long long length;
    Distance(): state(PS_DoNotExists), length(0) {}
};

struct Edge{
    int from;
    int to;
    long long dist;
    Edge() = default;
    Edge(int from, int to, long long dist): from(from), to(to), dist(dist) {}
};

class Graph {
public:
    explicit Graph(int n, int m): edges(m), graph(n, std::vector<Edge>()) {}
    void setEdge(int ind, int from, int to, long long d) {
        edges[ind] = Edge(from, to, d);
        graph[from].push_back(edges[ind]);
    }
    int size() const {
        return graph.size();
    } 
    const std::vector<Edge>& getEdges() const {
        return edges;
    }
    const std::vector<Edge>& getNeighbours(int v) const {
        return graph[v];
    }
private: 
    std::vector<Edge> edges; 
    std::vector<std::vector<Edge>> graph;
};

class GraphProcessor {
public:
    explicit GraphProcessor(const Graph& graph): graph(graph), dist(graph.size(), INF), used(graph.size(), false) {}
    std::vector<Distance> getDistances(int v) {
        findShortestPaths(v);
        std::vector<Distance> ans(graph.size());
        std::vector<long long> dist_main = dist;
        iter();
        for (int i = 0; i < graph.size(); i++) {
            if (dist[i] != dist_main[i]) {
                if (!used[i]) {
                    dfs(i);
                }
            }
        }
        for (int i = 0; i < graph.size(); i++) {
            if (used[i]) {
                ans[i].state = PS_IsInfinite;
            } else if (dist_main[i] == INF) {
                ans[i].state = PS_DoNotExists;
            } else {
                ans[i].state = PS_IsNormal;
                ans[i].length = dist[i];
            }
        }
        return ans; 
    }
private:
    const long long INF = std::numeric_limits<long long>::max();
    const Graph& graph;
    std::vector<long long> dist;
    std::vector<bool> used;
    void iter() {
        for (auto t: graph.getEdges()) {
            if (dist[t.from] != INF) {
                dist[t.to] = std::min(dist[t.to], dist[t.from] + t.dist);
            }
        }
    }
    void findShortestPaths(int v) {
        dist[v] = 0;
        for (int i = 0; i < graph.size() - 1; i++) {
            iter();
        }
    }
    void dfs(int v) {
        used[v] = true;
        for (auto edge: graph.getNeighbours(v)) {
            if (!used[edge.to]) {
                dfs(edge.to);
            }
        }
    }
};

main() {
    int n = 0, m = 0, s = 0;
    std::cin >> n >> m >> s;
    Graph g(n, m);
    for (int i = 0; i < m; i++) {
        int from = 0, to = 0;
        long long dist = 0;
        std::cin >> from >> to >> dist;
        g.setEdge(i, from - 1, to - 1, dist);
    }
    for (auto t: GraphProcessor(g).getDistances(s - 1)) {
        if (t.state == PS_IsNormal) { 
            std::cout << t.length << "\n";
        } else {
            std::cout << (t.state == PS_IsInfinite ? '-' : '*') << "\n";
        }
    }
}

