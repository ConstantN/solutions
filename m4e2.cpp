#include <iostream>
#include <vector>

/*
Задача:
Реализуйте структуру данных из n элементов a1,a2…an, поддерживающую следующие операции:
1. присвоить элементу ai значение j;
2. найти знакочередующуюся сумму на отрезке от l до r включительно (al−al+1+al+2−…±ar).

В первой строке входного файла содержится натуральное число n (1≤n≤10^5) — длина массива. Во второй строке записаны начальные значения элементов (неотрицательные целые числа, не превосходящие 10^4).

В третьей строке находится натуральное число m (1≤m≤10^5) — количество операций. В последующих m строках записаны операции:

операция первого типа задается тремя числами 0 i j (1≤i≤n, 1≤j≤10^4).
операция второго типа задается тремя числами 1 l r (1≤l≤r≤n).

Решение:
Дерево отрезков, в вершине храним знакочередующуюся сумму на отрезке
Комбинировать сумму с двух отрезков легко. Если на левом отрезке чётно элементов, то sum = L + R
Иначе sum = L - R
*/

class Tree {
public:
	
	Tree(const std::vector<int>& a): size(a.size()), a(a) {
		nodes = new int[4 * a.size() + 10];
		build(1, 0, size - 1);
	}
	void setValue(int ind, int value) {
		set(1, 0, size - 1, ind, value);
	}
	int getSum(int left, int right) {
		return get(1, 0, size - 1, left, right);
	}
	~Tree(){
		delete[] nodes;
	}
private:
	const int INF = 1e9;
	int size;
	int* nodes;
	std::vector<int> a;
	void build(int v, int tl, int tr);
	void set(int v, int tl, int tr, int ind, int val);
	int get(int v, int tl, int tr, int l, int r);
	int combine(int L, int sizeL, int R);
};

void Tree::build(int v, int tl, int tr) {
	if (tl == tr) {
		nodes[v] = a[tl];
		return;
	}
	int tm = (tl + tr) / 2;
	build(v * 2, tl, tm);
	build(v * 2 + 1, tm + 1, tr);
	nodes[v] = combine(nodes[v * 2], tm - tl + 1, nodes[v * 2 + 1]); 
}

void Tree::set(int v, int tl, int tr, int ind, int val) {
	if (tl == tr) {
		nodes[v] = val;
		return;
	}
	int tm = (tl + tr) / 2;
	if (ind <= tm) 
		set(v * 2, tl, tm, ind, val);
	else
		set(v * 2 + 1, tm + 1, tr, ind, val);
	nodes[v] = combine(nodes[v * 2], tm - tl + 1, nodes[v * 2 + 1]); 
}

int Tree::get(int v, int tl, int tr, int l, int r) {
	if (l > r)
		return 0;
	if (tl == l && tr == r)
		return nodes[v];
	int tm = (tl + tr) / 2;
	return combine(get(v * 2, tl, tm, l, std::min(tm, r)), std::max(std::min(tm, r) - l + 1, 0), get(v * 2 + 1, tm + 1, tr, std::max(tm + 1, l), r));
}

int Tree::combine(int L, int sizeL, int R) {
	return L + (sizeL % 2 == 0 ? R : -R);
}

main(){
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0;
	std::cin >> n;
	std::vector<int> v(n);
	for (auto &t : v) 
		std::cin >> t;
	Tree tree = Tree(v);
	int q = 0;
	std::cin >> q;
	while (q--) {
		int a = 0, b = 0, c = 0;
		std::cin >> a >> b >> c;
		if (a == 0) {
			tree.setValue(b - 1, c);
		} else {
			std::cout << tree.getSum(b - 1, c - 1) << "\n";
		}
	}
}
