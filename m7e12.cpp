#include <iostream>
#include <vector>
#include <cassert>

/*
Условие:
Компания BrokenTiles планирует заняться выкладыванием во дворах у состоятельных клиентов узор из черных и белых плиток, каждая из которых имеет размер 1×1 метр. Известно, что дворы всех состоятельных людей имеют наиболее модную на сегодня форму прямоугольника M×N метров.

Однако при составлении финансового плана у директора этой организации появилось целых две серьезных проблемы: во первых, каждый новый клиент очевидно захочет, чтобы узор, выложенный у него во дворе, отличался от узоров всех остальных клиентов этой фирмы, а во вторых, этот узор должен быть симпатичным.

Как показало исследование, узор является симпатичным, если в нем нигде не встречается квадрата 2×2 метра, полностью покрытого плитками одного цвета.

Для составления финансового плана директору необходимо узнать, сколько клиентов он сможет обслужить, прежде чем симпатичные узоры данного размера закончатся. Помогите ему!

Входные данные
На первой строке входного файла находятся три натуральных числа n, m, mod. 1≤n≤10100, 1≤m≤6, 1≤mod≤10000.

n, m — размеры двора. mod — модуль, по которому нужно посчитать ответ.

Выходные данные
Выведите в выходной файл единственное число — количество различных симпатичных узоров, которые можно выложить во дворе размера n×m по модулю mod. Узоры, получающиеся друг из друга сдвигом, поворотом или отражением считаются различными.

Решение:
dp[i][mask] - количество узоров ширины i, с последним рядом - mask.
Используем возведение матриц в степень, т.к. i может быть достаточно большим.
*/

int mod_2(const std::string& s) {
    return (s.back() - '0') % 2;
}

std::string plus(const std::string& s1, const std::string& s2) {
    int add = 0;
    std::string ans(s1.length(), '0');
    for (int i = s1.length() - 1; i >= 0; i--) {
        ans[i] = '0' + (((s1[i] - '0') + (s2[i] - '0') + add) % 10);
        add = ((s1[i] - '0') + (s2[i] - '0') + add > 9);
    }
    return ans;
}

std::string del_2(const std::string& s) {
    std::string s1(s.length(), '0');
    std::string s2(s.length(), '0');
    for (int i = 0; i < s.length(); i++) {
        s2[i] = '0' + ((s[i] - '0') / 2);
    }
    for (int i = 0; i < s.length() - 1; i++) {
        s1[i + 1] = '0' + 5 * ((s[i] - '0') % 2);
    }
    return plus(s1, s2);
}

std::string minus_1(const std::string& s) {
    std::string ans = s;
    for (int i = s.length() - 1; i >= 0; i--) {
        if (s[i] != '0') {
            ans[i] = s[i] - 1;
            break;
        } else {
            ans[i] = '9';
        }
    }
    return ans;
}

bool is_null(const std::string& s) {
    for (auto t : s) {
        if (t != '0') {
            return false;
        }
    }
    return true;
}

// ------------

int MOD;
 
int plus(int a, int b) {
    return (a + b) % MOD;
}
 
int mult(int a, int b) {
    return (a * b) % MOD;
}
 
class Matrix {
public:
    Matrix(int n, int m): n(n), m(m), value(n, std::vector<int>(m)) {}
    std::vector<int>& operator[](size_t i) {
        return value[i];
    }
    const std::vector<int>& operator[](size_t i) const {
        return value[i];
    }
    int getHeight() const {
        return n;
    }
    int getLength() const {
        return m;
    }
private:
    int n;
    int m;
    std::vector<std::vector<int>> value;
};
 
Matrix operator*(const Matrix& a, const Matrix& b) {
    if (a.getLength() != b.getHeight()) {
        assert(0);
    }
    Matrix ans(a.getHeight(), b.getLength());
    for (int i = 0; i < a.getHeight(); i++) {
        for (int j = 0; j < b.getLength(); j++) {
            for (int k = 0; k < a.getLength(); k++) {
                ans[i][j] = plus(ans[i][j], mult(a[i][k], b[k][j]));
            }
        }
    }
    return ans;
}
 
Matrix pow(const Matrix& m, const std::string& st) {
    if (is_null(st)) {
        Matrix E(m.getHeight(), m.getLength());
        for (int i = 0; i < m.getHeight(); i++) {
            E[i][i] = 1;
        }
        return E;
    }
    if (mod_2(st) == 0) {
        Matrix ans = pow(m, del_2(st));
        return ans * ans;
    }
    Matrix ans = pow(m, minus_1(st));
    return ans * m;
}

bool hasNeigbouringOne(int mask, int len) {
    bool prev = false;
    for (int i = 0; i < len; i++) {
        if (mask & (1 << i)) {
            if (prev) {
                return true;
            }
            prev = true;
        } else { 
            prev = false;
        }
    }
    return false;
}
 
bool isCompatibleMasks(int mask1, int mask2, int len) {
    int white = mask1 & mask2;
    int black = (~mask1) & (~mask2);
    return (!hasNeigbouringOne(white, len)) & (!hasNeigbouringOne(black, len));
}

long long countSequences(const std::string& n, int m) {
    const int masks = (1 << m);
    Matrix base(masks, 1);
    for (int i = 0; i < masks; i++) { 
        base[i][0] = 1;
    }
    Matrix move(masks, masks);
    for (int mask = 0; mask < masks; mask++) {
        for (int prev_mask = 0; prev_mask < masks; prev_mask++) { 
            move[mask][prev_mask] = isCompatibleMasks(mask, prev_mask, m);
        }
    }
    base = pow(move, minus_1(n)) * base;
    long long ret = 0;
    for (int i = 0; i < masks; i++) {
        ret = plus(ret, base[i][0]);
    }
    return ret;
}

int main() {
    int m = 0;
    std::string n;
    std::cin >> n >> m >> MOD;
    std::cout << countSequences(n, m);
}
