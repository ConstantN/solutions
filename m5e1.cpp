#include <iostream>
#include <vector>

/*
Ваша задача — написать программу, моделирующую простое устройство, которое умеет прибавлять целые значения к целочисленным переменным.

Входные данные
Входной файл состоит из одной или нескольких строк, описывающих операции. Строка состоит из названия переменной и числа, которое к этой переменной надо добавить. Все числа не превосходят 100 по абсолютной величине. Изначально все переменные равны нулю. Названия переменных состоят из не более чем 100000 маленьких латинских букв. Размер входного файла не превосходит 2 мегабайта.

Использован метод цепочек.
*/
 
struct Item {
    std::string key;
    int value;
    Item(const std::string& key, int value): key(key), value(value) {}
};

struct Id {
    int hash;
    int index;
    Id(int hash, int index): hash(hash), index(index) {}
};

class CustomMap {
public:
    CustomMap(): hash_table(size, std::vector<Item>()) {}
    int increment_and_return(const std::string& key, int value) {
        Id id = find(key);
        if (id.index == hash_table[id.hash].size()) {
            hash_table[id.hash].push_back(Item(key, 0));
        }
        hash_table[id.hash][id.index].value += value;
        return hash_table[id.hash][id.index].value;
    }
private:
    const long long mod = 1e9 + 7;
    const long long p = 101;
    const int size = 49999;
    std::vector<std::vector<Item>> hash_table;
    int h(const std::string& s) {
        long long ret = 0;
        long long P = 1;
        for (int i = 0; i < s.length(); i++) {
            ret = (ret + s[i] * P) % mod;
            P = (P * p) % mod;
        }
        return ret % size;
    }
    Id find(const std::string& s) {
        int hash = h(s);
        int i;
        for (i = 0; i < hash_table[hash].size(); i++) {
            if (hash_table[hash][i].key == s) {
                break;
            }
        }
        return Id(hash, i);
    }
};
 
int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    std::string s = "";
    int value = 0;
    CustomMap mp = CustomMap();
    while (std::cin >> s >> value) {
        std::cout << mp.increment_and_return(s, value) << "\n";
    }
}
