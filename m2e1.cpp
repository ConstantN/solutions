#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

/*
Задача - дано n длинных чисел, необходимо склеить их так, чтобы получилось одно наибольшее возможное число.
Решение:
Будем рассматривать числа как строки, тогда наша цель - получить лексикографически наибольшую строку.
Заметим, если А не явл. префиксом В и A < B, то стоит поставить В раньше чем А.
Но если А явл. префиксом В и А < В(автоматически), то не понятно, что из них ставить раньше.
Пример:
1) А = 123, B = 1230. AB = 1231230 > 1230123 = BA
2) A = 123, B = 1234, AB = 1231234 < 1234123 = BA
Как мы видим из примеров, в таких случаях важно, какие символы стоят в B, после префикса = А.
Для сравнения таких строк зациклим каждую строку.
Тогда проблема сравнения таких строк исчезнет, и вот почему:
--
Пусть А = А, B = AAA..AAC, т.е. у B сначала стоит сколько то строк А, а затем суффикс С.
Тогда нам необходимо понять, что лучше(больше лексикографически):
1) AB = AAAAAAAC
2) BA = AAAAAACA
Т.е. в действительности нужно понять, что больше - суффикс С или A.
Зацикливая A и B - сравним:
A' = AAAAAAAAAAAAAAAAAA...
B' = AAAAAAACAAAAAAACAA...
Т.е. по факту сравним - что больше суффикс С или А, что и требовалось.
--
Итого алгоритм: зациклим каждую строку, отсортируем по убыванию, выведем в этом порядке.
*/

int main() {
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	std::vector< std::pair<std::string, int> > numbers;
	std::string s = "";
	const int maxSize = 200;
	while (std::cin >> s) {
		int sizeReal = s.size();
		while (s.size() < maxSize) {
			int size = s.size();
			s += s[size % sizeReal];
		}
		numbers.emplace_back(s, sizeReal);
	}

	std::sort(numbers.rbegin(), numbers.rend());
	for (int i = 0; i < numbers.size(); ++i) {
		for (int j = 0; j < numbers[i].second; ++j) {
			std::cout << numbers[i].first[j];
		}
	}
}
