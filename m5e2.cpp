#include <iostream>
#include <vector>

/*
Пусть все натуральные числа исходно организованы в список в естественном порядке. Разрешается выполнить следующую операцию: swap(a,b). Эта операция возвращает в качестве результата расстояние в текущем списке между числами a и b и меняет их местами.

Задана последовательность операций swap. Требуется вывести в выходной файл результат всех этих операций.

Входные данные
Первая строка входного файла содержит число n (1⩽n⩽200000) — количество операций. Каждая из следующих n строк содержит по два числа в диапазоне от 1 до 109 — аргументы операций swap.

Использован метод цепочек
*/
 
struct Item {
    int key;
    int value;
    Item(int key, int value): key(key), value(value) {}
};
 
struct Id {
    int hash;
    int index;
    Id(int hash, int index): hash(hash), index(index) {}
};

class CustomMap {
public:
    CustomMap(): hash_table(size, std::vector<Item>()) {}
    int swap_values(int a, int b) {
        Id idA = touch(a);
        Id idB = touch(b);
        std::swap(hash_table[idA.hash][idA.index].value, hash_table[idB.hash][idB.index].value);
        return std::abs(hash_table[idA.hash][idA.index].value - hash_table[idB.hash][idB.index].value);
    }
private:
    const long long a = 997;
    const long long b = 991;
    const int size = 49999;
    std::vector<std::vector<Item>> hash_table;
    int h(int key) {
        return (a * key + b) % size;
    }
    Id find(int key) {
        int hash = h(key);
        int i;
        for (i = 0; i < hash_table[hash].size(); i++) {
            if (hash_table[hash][i].key == key) {
                break;
            }
        }
        return Id(hash, i);
    }
    Id touch(int key) {
        Id id = find(key);
        if (id.index == hash_table[id.hash].size()) {
            hash_table[id.hash].push_back(Item(key, key));
        }
        return id;
    }
};
 
int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    CustomMap mp = CustomMap();
    int q = 0;
    std::cin >> q;
    int a = 0;
    int b = 0;
    while (q--) {
        std::cin >> a >> b;
        std::cout << mp.swap_values(a, b) << "\n";
    }
}
