#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

/*
Задача - реализовать струкутуру, позволяющую отвечать на запросы extractMin, extractMax, insert.
Решение - реализуем одну структуру с флагом isMin, чтобы понимать, это куча на максимум или минимум.
Создадим две кучи - на минимум и на максимум.
insert - реализуется очевидно, просто добавим элемент в обе кучи
extractMin = MinHeap.extractTop + MaxHeap.eraseByKey
extarctMax = MaxHeap.extractTop + MinHeap.eraseByKey

extractTop - очевидная реализация. Разберемся с eraseByKey.

Утверждение - каждый раз когда бы будем делать eraseByKey - му будем удалять лист в соотв. дереве.
Для определённости пусть делаем MaxHeap.eraseByKey(extractedMin)
Очевидно extractedMin является листом, т.к. на его поддереве нет элементов, т.к. он строго наименьший в множестве.

Из данного утверждения назовём функцию eraseByKey = eraseLeaf и реализуем её, удаляя этот элемент-лист явно.

Особенность - поскольку требуется реализовать мультимножество, то в вершине будем хранить не только значение,
но и количество раз, сколько это значение лежит в куче. Это ухищрение необходимо для верности Утверждения.

Особенность - решение в оффлайне. Т.е. я перенумеровываю все значения во входных,сохраняя их относительный порядок,
но делая их последовательными натуральными числами. Это необходимо, чтобы ключ мог быть по совместительству индексом
в массиве index, чтобы в случае необходимости можно было получать индекс вершины по ключу.
*/

struct Heap{
	const int maxSize = 1e5 + 1;
	std::vector<std::pair<int, int>> nodes; //{value, numberOfValues}
	std::vector<int> index; //index[value] - is such i, that nodes[i].first = value
	int size;
	bool isMin;
};

int sign(Heap* heap) {
	if(heap->isMin) return 1;
	return -1;
}

void swapNodes(Heap* heap, int index1, int index2) {
	heap->index[heap->nodes[index1].first] = index2;
	heap->index[heap->nodes[index2].first] = index1;
	std::swap(heap->nodes[index1], heap->nodes[index2]);
}

void initHeap(Heap* heap, bool isMin) {
	heap->nodes.resize(heap->maxSize);
	heap->index.resize(heap->maxSize);
	heap->size = 0;
	heap->isMin = isMin;
}

void siftUp(Heap* heap, int vertex) {
	while (vertex / 2 > 0 && heap->nodes[vertex].first * sign(heap) < heap->nodes[vertex / 2].first * sign(heap)) {
		swapNodes(heap, vertex, vertex / 2);
		vertex /= 2;
	}
}

void siftDown(Heap* heap, int vertex) {
	while (vertex * 2 <= heap->size){
		int where = -1;
		if (heap->nodes[vertex].first * sign(heap) > heap->nodes[vertex * 2].first * sign(heap)) {
			where = vertex * 2;
		}
		if (vertex * 2 + 1 <= heap->size && heap->nodes[vertex].first * sign(heap) > heap->nodes[vertex * 2 + 1].first * sign(heap) && (where == -1 || heap->nodes[vertex * 2 + 1].first * sign(heap) < heap->nodes[vertex * 2].first * sign(heap))){
			where = vertex * 2 + 1;
		}
		if (where == -1) {
			break;
		}
		swapNodes(heap, vertex, where);
		vertex = where;
	}
}

void clearNode(Heap* heap, int index) {
	heap->index[heap->nodes[index].first] = 0;
	heap->nodes[index] = {0, 0};
}

void swapWithLast(Heap* heap, int index) {
	swapNodes(heap, index, heap->size);
}

int extractTop(Heap* heap) {
	int value =  heap->nodes[1].first;
	if (--heap->nodes[1].second > 0) {
		return value;
	}

	swapWithLast(heap, 1);
	clearNode(heap, heap->size);
	--heap->size;
	
	if (heap->size > 0) {
		siftDown(heap, 1);
	}
	return value;
}

void eraseLeaf(Heap* heap, int value) {
	int index = heap->index[value];
	if (--heap->nodes[index].second > 0) {
		return;
	}
	if(index != heap->size){
		swapWithLast(heap, index);
	}
	clearNode(heap, heap->size);
	--heap->size;
	if(index != heap->size + 1){
		siftUp(heap, index);
	}
}

void insert(Heap* heap, int value) {
	if (heap->index[value] == 0) {
		heap->index[value] = ++heap->size;
		heap->nodes[heap->size] = {value, 1};
		siftUp(heap, heap->size);
	} else {
		++heap->nodes[heap->index[value]].second;
	}
}

std::vector<int> solve(std::pair<char,int>* queries, int n) {
	std::vector<int> inserts;
	for (int i = 0; i < n; ++i) {
		auto query = queries[i];
		if (query.first == 'i') {
			inserts.push_back(query.second);
		}
	} 
	sort(inserts.begin(), inserts.end());
	std::vector<int> numbers;
	for (auto t : inserts) {
		if (numbers.empty() || numbers.back() != t) {
			numbers.push_back(t);
		}
	} 
	
	Heap maxHeap = Heap();
	initHeap(&maxHeap, 0);
	Heap minHeap = Heap();
	initHeap(&minHeap, 1);
	std::vector<int> ans;

	for (int i = 0; i < n; ++i) {
		auto query = queries[i];
		if (query.first == 'i') { //insert
			int l = -1;
			int r = numbers.size();
			while (l + 1 < r) {
				int m = (l + r) / 2;
				if (numbers[m] < query.second) {
					l = m;
				} else {
					r = m;
				}
			}
			insert(&minHeap, r);
			insert(&maxHeap, r);
		} else if (query.first == 'n') { //getMin
			int extracted = extractTop(&minHeap); 
			ans.push_back(numbers[extracted]);
			eraseLeaf(&maxHeap, extracted);
		} else if (query.first == 'x') { //getMax
			int extracted = extractTop(&maxHeap);
			ans.push_back(numbers[extracted]);
			eraseLeaf(&minHeap, extracted);
		}
	}
	return ans;
}

int main() {
	int n = 0;
	std::cin >> n;
	std::pair<char, int>* queries = new std::pair<char, int>[n];
	for (int i = 0; i < n; ++i) {
		char c = (char)0;
		std::cin >> c;
		if (c == 'I') {
			while (true) {
				if (c == '(') {
					break;
				}
				std::cin >> c;
			}
			int value = 0;
			while (true) {
				std::cin >> c;
				if (c == ')') {
					break;
				}
				value = value * 10 + (c - '0');
			}
			queries[i] = {'i', value};
		} else if (c == 'G') {
			std::string s = "";
			std::cin >> s;
			queries[i] = {s.back(), 0};
		}
	}
	std::vector<int> ans = solve(queries, n);
	for (auto t : ans) {
		std::cout << t << "\n";
	}		
}
