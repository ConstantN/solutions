#include <iostream>
#include <vector>

/* 
Напишите программу, которая для двух вершин дерева определяет, является ли одна из них предком другой.

Входные данные
Первая строка входного файла содержит натуральное число n (1≤n≤100000) — количество вершин в дереве.

Во второй строке находится n чисел. При этом i-е число второй строки определяет непосредственного родителя вершины с номером i. Если номер родителя равен нулю, то вершина является корнем дерева.

В третьей строке находится число m (1≤m≤100000)  — количество запросов. Каждая из следующих m строк содержит два различных числа a и b (1≤a,b≤n).

Выходные данные
Для каждого из m запросов выведите на отдельной строке число 1, если вершина a является одним из предков вершины b, и 0 в противном случае.

Решение:
Заметим, А явл. предком B <=> (tin[A] < tin[B]) and (tout[A] > tout[B]) 
За 1 dfs обход дерева посчитаем все tin и tout, затем будем отвечать за О(1) на запросы.
*/

class Graph{
public:
    explicit Graph(int n): edges(n, std::vector<int>()) {}
    void addEdge(int son, int parent) {
        edges[parent].push_back(son);
    } 
    const std::vector<int>& getNeighbours(int v) const {
        return edges[v];
    }
    int getSize() const {
        return edges.size();
    }
private:
    std::vector<std::vector<int>> edges; 
};

class GraphProcessor {
public:
    explicit GraphProcessor(const Graph& graph, int root): graph(graph), timer(0),
                            tin(graph.getSize(), 0), tout(graph.getSize(), 0) {
        dfs(root);
    } 
    bool isParent(int son, int parent) {
        return (tin[parent] < tin[son]) && (tout[parent] > tout[son]);
    }
private:
    const Graph& graph;
    int timer;
    std::vector<int> tin;
    std::vector<int> tout;
    void dfs(int v) {
        tin[v] = timer++;
        for (auto t : graph.getNeighbours(v)) {
            dfs(t);
        }
        tout[v] = timer++;
    }
};

void findParents(int n, const std::vector<int>& parents, int q,
                    const std::vector<std::pair<int,int>>& queries, std::vector<bool>& ans) {
    Graph graph(n);
    int root = -1;
    for (int i = 0; i < n; i++) {
        if (parents[i] == 0) {
            root = i;
            continue;
        }
        graph.addEdge(i, parents[i] - 1);
    }
    GraphProcessor graphProcessor(graph, root);
    for (int i = 0; i < q; i++) {
        ans[i] = graphProcessor.isParent(queries[i].second - 1, queries[i].first - 1);
    }
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0;
    std::cin >> n;
    std::vector<int> parents(n);
    for (auto &t : parents) {
        std::cin >> t;
    }
    int q = 0;
    std::cin >> q;
    std::vector<std::pair<int,int>> queries(q);
    for (auto &t : queries) {
        std::cin >> t.first >> t.second;
    }
    std::vector<bool> ans(q);
    findParents(n, parents, q, queries, ans);
    for (auto t : ans) {
        std::cout << t << "\n";
    }
}
