#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>

/*
Условие:
Продавец аквариумов для кошек хочет объехать n городов, посетив каждый из них ровно один раз. Помогите ему найти кратчайший путь.

Входные данные
Первая строка входного файла содержит натуральное число n (1⩽n⩽13) — количество городов. Следующие n строк содержат по n чисел — длины путей между городами.

В i-й строке j-е число — ai,j — это расстояние между городами i и j (0⩽ai,j⩽106; ai,j=aj,i; ai,i=0).

Выходные данные
В первой строке выходного файла выведите длину кратчайшего пути. Во второй строке выведите n чисел — порядок, в котором нужно посетить города.

Решение:
dp[i][mask] - минимальная сумма, чтобы посетить вершины из mask, последней - i-ую.
*/

int solve(int n, const std::vector<std::vector<int>>& v, std::vector<int>& ans) {
    const int masks = (1 << n), INF = 1e9;
    std::vector<std::vector<int>> dp(n, std::vector<int>(masks, INF));
    std::vector<std::vector<int>> p(n, std::vector<int>(masks, INF));
    for (int i = 0; i < n; i++) {
        dp[i][(1 << i)] = 0;
    }
    for (int mask = 0; mask < masks; mask++) {
        for (int i = 0; i < n; i++) {
            if (mask & (1 << i) == 0) {
                continue;
            }
            int nwmask = mask ^ (1 << i);
            for (int j = 0; j < n; j++) {
                if (nwmask & (1 << j) == 0) {
                    continue;
                }
                if (dp[i][mask] > dp[j][nwmask] + v[i][j]) {
                    dp[i][mask] = dp[j][nwmask] + v[i][j];
                    p[i][mask] = j;
                }
            }
        }
    }
    int ret = INF;
    int best_id = -1;
    for (int i = 0; i < n; i++) {
        if (ret > dp[i][masks - 1]) {
            ret = dp[i][masks - 1];
            best_id = i;
        }
    }
    int mask = masks - 1;
    while (best_id != INF) {
        ans.push_back(best_id);
        int nwmask = mask ^ (1 << best_id);
        best_id = p[best_id][mask];
        mask = nwmask;
    }
    std::reverse(ans.begin(), ans.end());
    return ret;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0;
    std::cin >> n;
    std::vector<std::vector<int>> v(n, std::vector<int>(n));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cin >> v[i][j];
        }
    }
    std::vector<int> ans;
    std::cout << solve(n, v, ans) << "\n";
    for (auto t : ans) {
        std::cout << t + 1 << " ";
    }
}
