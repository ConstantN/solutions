#include <iostream>
#include <vector>
#include <algorithm>

/*
Задача - дано n отрезков на числовой прямой, необоходимо найти сумму длин частей числовой прямой, покрытых ровно одним отрезком из числа данных.
Решение:
Используем сканлайн. Создадим события открытия отрезков и закрытия отрезков. Отсортируем их.
Создадим переменную counter, в которой будем поддерживать - сколько отрезков в данный момент открыты.
Будем обрабатывать события по возрастанию координаты и добавлять в ans длину последнего подотрезка,
если на нём counter = 1. 
*/

int main() {
	int n = 0;
	std::cin >> n;
	std::vector< std::pair<int, int> > line(2 * n, {0, 0});
	for (int i = 0; i < n; ++i) {
		int L = 0, R = 0;
		std::cin >> L >> R;
		line[2 * i] = {L, 1};
		line[2 * i + 1] = {R, -1};
	}
	
	std::sort(line.begin(), line.end());
	int counter = 0;
	int ans = 0;
	int last = -1;
	for (int i = 0; i < line.size(); ++i) {
		if (counter == 1) {
			ans += line[i].first - last;
		}
		last = line[i].first;
		counter += line[i].second;
	}
	std::cout << ans;
}
