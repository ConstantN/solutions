#include <iostream>
#include <vector>
#include <algorithm>

/*
Дан ориентированный невзвешенный граф без петель и кратных рёбер. Необходимо определить есть ли в нём циклы, и если есть, то вывести любой из них.

Входные данные
В первой строке входного файла находятся два натуральных числа N и M (1⩽N⩽100000, M⩽100000) — количество вершин и рёбер в графе соответственно. Далее в M строках перечислены рёбра графа. Каждое ребро задаётся парой чисел  — номерами начальной и конечной вершин соответственно.

Выходные данные
Если в графе нет цикла, то вывести «NO», иначе  — «YES» и затем перечислить все вершины в порядке обхода цикла.

Решение: 
Найдём цикл, запуская dfs из каждой вершины.
Цель - поддерживать стэк и уметь проверять, что вершина лежит в стеке(ищем Ро-подграф).
Это осуществляется fast-forward и через массив used, соответственно.
used[v] = 0, если не посещали
= 1, если в стеке лежит
= 2, если посетили и выбросили из стека 
Затем восстановим цикл из стека.
*/

class Graph{
public:
    explicit Graph(int n): edges(n, std::vector<int>()) {}
    void addEdge(int from, int to) {
        edges[from].push_back(to);
    }
    const std::vector<int>& getFriends(int v) const {
        return edges[v];
    } 
    int getSize() const {
        return edges.size();
    }
private:
    std::vector<std::vector<int>> edges;    
};

void hasCycle(int v, const Graph& g, std::vector<int>& stack, std::vector<int>& used, bool& found) {
    stack.push_back(v);
    used[v] = 1;
    for (auto t: g.getFriends(v)) {
        if (used[t] || found) {
            if (used[t] == 1 && !found) {
                stack.push_back(t);
                found = true;
            }
            continue;
        }
        hasCycle(t, g, stack, used, found);
    }
    used[v] = 2;
    if (!found) {
        stack.pop_back();
    }
}

void formCycle(const std::vector<int>& stack, std::vector<int>& ans) {
    int size = stack.size();
    if (stack.size() == 0) {
        return;
    }
    int begin = stack.back();
    ans.push_back(begin);
    for (int i = size - 2; stack[i] != begin; i--) {
        ans.push_back(stack[i]);
    }
    std::reverse(ans.begin(), ans.end());
}

bool hasCycle(const Graph& g, std::vector<int>& ans) {
    std::vector<int> stack;
    std::vector<int> used(g.getSize());
    bool found = false;
    for (int i = 0; i < g.getSize(); i++) {
        if (!used[i] && !found) {
            hasCycle(i, g, stack, used, found);
        }
    }
    formCycle(stack, ans);
    return ans.size();
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0, m = 0;
    std::cin >> n >> m; 
    Graph g(n);
    for (int i = 0; i < m; i++) {
        int a = 0, b = 0;
        std::cin >> a >> b;
        g.addEdge(a - 1, b - 1); 
    }
    std::vector<int> ans;
    if (hasCycle(g, ans)) {
        std::cout << "YES\n";
        for (auto t : ans) {
            std::cout << t + 1 << " ";
        }
    } else {
        std::cout << "NO\n";
    }    
}
