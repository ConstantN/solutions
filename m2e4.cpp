#include <iostream>
#include <vector>

/*
Задача - реализовать кучу на минимум с getMin, eraseMin, decreaseKey, insert.
Решение - стандартная реализация кучи.
Особенность - в структуре кучи я также храню массив int query[], где в query[i] лежит индекс
добавленной в i-ом запросе вершины в массиве nodes. В самой вершине nodes также храним индекс запроса, на котором
была добавлена эта вершина. Это позволяет поддерживать массив query валидным, а он нужен чтобы при decreaseKey
обращаться к нужной вершине для уменьшения ключа.  
*/

struct Heap{
	std::pair<long long, int>* nodes; //{value, queryIndex}
	int* query;
	int size;
};

void initHeap(Heap* heap, int maxSize) {
	heap->nodes = new std::pair<long long, int>[maxSize];
	heap->query = new int[maxSize];
	heap->size = 0;
}

void eraseHeap(Heap* heap) {
	delete[] heap->nodes;
	delete[] heap->query;
}

void swapNodes(Heap* heap, int vertex1, int vertex2) {
	std::swap(heap->query[heap->nodes[vertex1].second], heap->query[heap->nodes[vertex2].second]);
	std::swap(heap->nodes[vertex1], heap->nodes[vertex2]);
}

void siftDown(Heap* heap, int vertex) {	
	while (2 * vertex <= heap->size) {
		int left = 2 * vertex;
		int right = 2 * vertex + 1;
		int where = -1;
		if (heap->nodes[left].first < heap->nodes[vertex].first) {
			where = left;
		}
		if (right <= heap->size && heap->nodes[right].first < heap->nodes[vertex].first && (where == -1 || heap->nodes[right] < heap->nodes[left])) {
			where = right;
		}
		if (where == -1) {
			break;
		}
		swapNodes(heap, vertex, where);
		vertex = where;
	}
}

void siftUp(Heap* heap, int vertex) {
	int parent = vertex / 2;
	while (parent > 0 && heap->nodes[vertex].first < heap->nodes[parent].first) {
		swapNodes(heap, vertex, parent);
		vertex = parent;
		parent /= 2;
	}
}

void insert(Heap* heap, int val, int qIndex) {
	heap->nodes[++heap->size] = {val, qIndex};
	heap->query[qIndex] = heap->size;
	siftUp(heap, heap->size);
}

long long getMin(Heap* heap) {
	return heap->nodes[1].first;
}

void eraseMin(Heap* heap) {
	if (heap->size == 0) {
		return;
	}
	swapNodes(heap, 1, heap->size);
	heap->query[heap->nodes[heap->size].second] = 0;
	heap->nodes[heap->size--] = {0ll, 0};
	if (heap->size > 0) {
		siftDown(heap, 1);
	}
}

void decreaseVal(Heap* heap, int qIndex, int dt) {
	heap->nodes[heap->query[qIndex]].first -= dt;
	siftUp(heap, heap->query[qIndex]);
}

int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);	
	int n = 0;
	std::cin >> n;
	Heap heap = Heap();
	initHeap(&heap, n + 1);
	for (int i = 1; i <= n; ++i) {
		std::string q = "";
		std::cin >> q;
		if (q[0] == 'i') {
			int val = 0;
			std::cin >> val;
			insert(&heap, val, i);
		} else if (q[0] == 'g') {
			std::cout << getMin(&heap) << "\n";
		} else if (q[0] == 'e') {
			eraseMin(&heap);
		} else if (q[0] == 'd') {
			int qIndex = 0, dt = 0;
			std::cin >> qIndex >> dt;
			decreaseVal(&heap, qIndex, dt);
		}
	}
	eraseHeap(&heap);
}
