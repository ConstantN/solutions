#include <iostream>
#include <vector>
#include <cassert>

/*
Условие:
International Biology Manufacturer (IBM) обнаружили, что органический материал на Марсе имеет ДНК, состоящий из 5 символов(a,b,c,d,e), вместо четырех компонентов ДНК на Земле. Однако в строке не может встречаться ни одна из следующих пар в качестве подстроки: cd, ce, ed и ee.

IBM заинтересовались сколько правильных Марсианских строк ДНК длины n возможно составить?

Входные данные
Входные данные содержат несколько тестов. Каждый тест содержит одно число n на отдельной строке. Последняя строка входных файлов содержит ноль.

Количество тестов не превосходит 100.
Число n лежит в пределах от 1 до 250 включительно.
Выходные данные
Для каждого теста выведите на отдельной строке количество правильных строк по модулю 999999937

Решение:
dp[i][j] - количество последовательностей длины i, оканчивающихся на j-ую букву.
Используем умножение матриц, т.к. i может быть очень большим.
*/

const int MOD = 999999937;
 
int plus(int a, int b) {
    return (a + b) % MOD;
}
 
int mult(int a, int b) {
    return (1ll * a * b) % MOD;
}
 
class Matrix {
public:
    Matrix(int n, int m): n(n), m(m), value(n, std::vector<int>(m)) {}
    std::vector<int>& operator[](size_t i) {
        return value[i];
    }
    const std::vector<int>& operator[](size_t i) const {
        return value[i];
    }
    int getH() const {
        return n;
    }
    int getL() const {
        return m;
    }
private:
    int n;
    int m;
    std::vector<std::vector<int>> value;
};
 
Matrix operator*(const Matrix& a, const Matrix& b) {
    if (a.getL() != b.getH()) {
        assert(0);
    }
    Matrix ans(a.getH(), b.getL());
    for (int i = 0; i < a.getH(); i++) {
        for (int j = 0; j < b.getL(); j++) {
            for (int k = 0; k < a.getL(); k++) {
                ans[i][j] = plus(ans[i][j], mult(a[i][k], b[k][j]));
            }
        }
    }
    return ans;
}
 
Matrix pow(const Matrix& m, long long st) {
    if(st == 0) {
        Matrix E(m.getH(), m.getL());
        for (int i = 0; i < m.getH(); i++) {
            E[i][i] = 1;
        }
        return E;
    }
    if(st % 2 == 0) {
        Matrix ans = pow(m, st / 2);
        return ans * ans;
    }
    Matrix ans = pow(m, st - 1);
    return ans * m;
}

int countSequences(long long n) {
    Matrix move(5, 5), base(5, 1);
    for (int i = 0; i < 5; i++) {
        base[i][0] = 1;
        for (int j = 0; j < 5; j++) {
            move[i][j] = 1;
        }
    }
    move[3][2] = move[3][4] = move[4][4] = move[4][2] = 0;
    base = pow(move, n - 1) * base;
    int ans = 0;
    for (int i = 0; i < 5; i++) {
        ans = plus(ans, base[i][0]);
    }
    return ans;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    while (true) {
        long long n;
        std::cin >> n;
        if (n == 0) {
            break;
        }
        std::cout << countSequences(n) << "\n";
    }
}
