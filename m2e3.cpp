#include <iostream>
#include <vector>

/*
Задача - дан массив неотрицательных целых чисел. Необходимо за O(n) найти в нём k-ый по возрастанию элемент, считая с нуля.
Решение:

Функция partition(v,left,right) рассматривает отрезок [l,r] в массиве v.
Выбирает некоторый элемент и переставляет все меньшие его - левее его, все большие - правее.
Возвращает индекс этого разделителя.

Функция findKth находит K-ый по возрастанию элемент(считая с единицы) в массиве v.
Возвращает его значение.
*/

int partition(std::vector<int> &v, int left, int right) { //Left < pivot <= Right, returns index of pivot
	int ptrL = left; //pointer like .end() of Left part
	std::swap(v[right], v[left + rand() % (right - left + 1)]);
	int pivotInd = right;
	for (int i = left; i < right; ++i) {
		if (v[i] < v[pivotInd]) {
			std::swap(v[i], v[ptrL]);
			++ptrL;	
		}
	}
	std::swap(v[ptrL], v[pivotInd]);
	return ptrL;
}

int findKth(std::vector<int> &v, int n, int k) {
	int left = 0, right = n - 1;
	while (left < right) {
		int indPivot = partition(v, left, right);
		if (indPivot - left >= k) {
			right = indPivot - 1;
		} else {
			k -= indPivot - left;
			left = indPivot;
		}
	}
	return v[left];
}

int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0, k = 0;
	std::cin >> n >> k;
	std::vector<int> v(n, 0);
	for (int i = 0; i < n; ++i) {
		std::cin >> v[i];
	}
	std::cout << findKth(v, n, k + 1);		
}
