#include <iostream>
#include <cstring>

class String{
public:
	String();
	String(const char* _value);
	String(size_t n, char c);
	String(const String& s);
	String(char c);
	~String();
	String& operator=(String s);
	bool operator==(const String& s) const;
	String& operator+=(const String& s);
	const char& operator[](size_t index) const;
	char& operator[](size_t index);
	size_t length() const;
	void push_back(const char c);
	void pop_back();
	char& front();
	const char& front() const;
	char& back();
	const char& back() const;
	size_t find(const String& s, bool r) const;
	size_t rfind(const String& s) const;
	String substr(size_t start, size_t count) const;
	bool empty() const;
	void clear();	
private:
	static constexpr double EXPANSION_COEFFICIENT = 2;
	size_t bufferSize;
	size_t realSize;
	char* value;
	void swap(String& s);
	void reallocate();	
};

String::String() {
	realSize = 0;
	bufferSize = 1;
	value = new char[bufferSize];
}

String::String(const char* _value) {
	realSize = strlen(_value);
	bufferSize = EXPANSION_COEFFICIENT * realSize + 1;
	value = new char[bufferSize];
	memcpy(value, _value, realSize);
}

String::String(size_t n, char c) {
	realSize = n;
	bufferSize = EXPANSION_COEFFICIENT * realSize + 1;	
	value = new char[bufferSize];
	memset(value, c, n);
}

String::String(const String& s) {
	realSize = s.length();
	bufferSize = EXPANSION_COEFFICIENT * realSize + 1;	
	value = new char[bufferSize];
	memcpy(value, s.value, realSize);
}

String::String(char c): String(1, c) {
}

String::~String() {
	delete[] value;
}

String& String::operator=(String s) {
	swap(s);
	return *this;
}

bool String::operator==(const String& s) const {
	if (s.realSize != realSize)
		return false;
	for (size_t i = 0; i < realSize; ++i)
		if(s.value[i] != value[i])
			return false;
	return true;
}

String& String::operator+=(const String& s) {
	size_t size = s.realSize;
	for (size_t i = 0; i < size; ++i)
		push_back(s.value[i]);
	return *this;
}

String operator+(const String& a, const String& b) {
	String copy = a;
	return copy += b;
}

const char& String::operator[](size_t index) const {
	return value[index];
}

char& String::operator[](size_t index) {
	return value[index];
}

std::ostream& operator<<(std::ostream& out, const String& s) {
	for (size_t i = 0; i < s.length(); ++i) {
		out << s[i];
	}
	return out; 
}

std::istream& operator>>(std::istream& in, String& s) {
	s = String();
	char c;
	while (in.get(c)) {
		if (c == ' ' || c == '\n') 
			break;
		s.push_back(c);
	}
	return in;
}

size_t String::length() const {
	return realSize;
}

void String::push_back(const char c) {
	if (realSize == bufferSize) 
		reallocate();
	value[realSize++] = c;
}

void String::pop_back() {
	if (realSize == 0)
		return;
	realSize--;
}

char& String::front() {
	return value[0];
}

const char& String::front() const {
	return value[0];
}

char& String::back() {
	return value[realSize - 1];
}

const char& String::back() const {
	return value[realSize - 1];
}

size_t String::find(const String& s, bool r = false) const {
	size_t index = realSize;
	for (size_t i = 0; i <= realSize - s.realSize; ++i) {
		bool found = true;
		for (size_t j = 0; j < s.realSize; ++j) {
			if (s.value[j] != value[i + j]) {
				found = false;
				break;	
			}
		}
		if (found) {
			index = i;
			if (!r)
				return index;
		}
	}
	return index;
}

size_t String::rfind(const String& s) const {
	return find(s, true);
}

String String::substr(size_t start, size_t count) const {
	String s = String(count, '\0');
	for (size_t i = 0; i < count; ++i) {
		s.value[i] = value[start + i];
	}
	return s;	
}

bool String::empty() const {
	return realSize == 0;
}

void String::clear() {
	*this = String();
}

void String::swap(String& s) {
	std::swap(bufferSize, s.bufferSize);
	std::swap(realSize, s.realSize);
	std::swap(value, s.value);
}

void String::reallocate() {
	if (realSize == bufferSize) {	
		bufferSize *= EXPANSION_COEFFICIENT;
		char* newValue = new char[bufferSize];
		memcpy(newValue, value, realSize);
		delete[] value;
		value = newValue; 
	} else if (realSize < bufferSize / EXPANSION_COEFFICIENT / EXPANSION_COEFFICIENT) {
		bufferSize /= EXPANSION_COEFFICIENT;
		char* newValue = new char[bufferSize];
		memcpy(newValue, value, realSize);
		delete[] value;
		value = newValue;
	}
}
