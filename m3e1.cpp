#include <iostream>
#include <string>

using std::cin;
using std::cout;

/*
Входной файл содержит описание операций с деревом. Операций не больше 105.

В каждой строке находится одна из следующих операций:

insert x — добавить в дерево ключ x.
delete x — удалить из дерева ключ x. Если ключа x в дереве нет, то ничего делать не надо.
exists x — если ключ x есть в дереве, вывести «true», иначе «false»
next x — минимальный элемент в дереве, больший x, или «none», если такого нет.
prev x — максимальный элемент в дереве, меньший x, или «none», если такого нет.
*/

struct Node {
	int value;
	int height;
	Node* l;
	Node* r;
	Node(int value): value(value), height(1), l(nullptr), r(nullptr){}
	int getValue();
	static int getHeight(Node* node);
	static int getBalance(Node* node);
	static void fixHeight(Node* node);
	~Node() {
		if(l) delete l;
		if(r) delete r;
	}
};

int Node::getValue() {
	return this->value;
}

int Node::getHeight(Node* node) {
	if (!node)
		return 0;
	return node->height;
}

int Node::getBalance(Node* node) {
	if(!node)
		return 0;
	return getHeight(node->r) - getHeight(node->l);
}

void Node::fixHeight(Node* node) {
	if(!node)
		return;
	node->height = std::max(Node::getHeight(node->l), Node::getHeight(node->r)) + 1;
}

class AVLTree {
public:
	AVLTree(): root(nullptr) {}
	Node* next(int value);
	Node* prev(int value);
	void insert(int value);
	void erase(int value);
	bool find(int value);
	~AVLTree() {
		if(root) delete root;
	}
private:
	static Node* rotateLeft(Node* node);
	static Node* rotateRight(Node* node);
	static Node* rotate(Node* node);
	static Node* next(Node* node, int value);
	static Node* prev(Node* node, int value);
	static Node* insert(Node* node, int value);
	static Node* erase(Node* node, int value);
	static bool find(Node* node, int value);
	Node* root;
};

Node* AVLTree::next(int value) {
	return next(root, value);
}

Node* AVLTree::prev(int value) {
	return prev(root, value);
}

bool AVLTree::find(int value) {
	return find(root, value);
}

void AVLTree::insert(int value) {
	root = insert(root, value);
}

void AVLTree::erase(int value) {
	root = erase(root, value);
}

Node* AVLTree::rotateLeft(Node* node) { 
	Node* top = node->r;
	node->r = top->l;	
	top->l = node;
	Node::fixHeight(node);
	Node::fixHeight(top);
	return top;
}
 
Node* AVLTree::rotateRight(Node* node) {
	Node* top = node->l;
	node->l = top->r;
	top->r = node;
	Node::fixHeight(node);
	Node::fixHeight(top);
	return top;
}

Node* AVLTree::rotate(Node* node) {	
	Node::fixHeight(node);
	if (Node::getBalance(node) == 2) {
		if (Node::getBalance(node->r) < 0) 
			node->r = rotateRight(node->r);
		return rotateLeft(node);
	} else if (Node::getBalance(node) == -2) {
		if (Node::getBalance(node->l) > 0) 
			node->l = rotateLeft(node->l);
		return rotateRight(node);
	}
	return node;
}

bool AVLTree::find(Node* node, int value) {
	if (!node) 
		return false;
	if (node->value == value)
		return true;
	if (node->value < value)
		return find(node->r, value);
	return find(node->l, value);
}

Node* AVLTree::insert(Node* node, int value) {
	if (!node)
		return new Node(value);
	if (node->value < value)
		node->r = insert(node->r, value);	
	else if (node->value > value)
		node->l = insert(node->l, value);
	return rotate(node);
}

Node* AVLTree::erase(Node* node, int value) {
	if (!node) 
		return nullptr;
	if (node->value < value) {
		node->r = erase(node->r, value);
		return rotate(node);
	}
	if (node->value > value) {
		node->l = erase(node->l, value);
		return rotate(node);
	}
	if(!node->r) {
		auto L = node->l;
		node->l = node->r = nullptr;
		delete node;
		return L; 
	}
	auto top = next(node->r, value);
	node->value = top->value;
	node->r = erase(node->r, top->value);  
	return rotate(node);
}

Node* AVLTree::next(Node* node, int value) {
	if (!node) 
		return nullptr;
	if (node->value	<= value) {
		return next(node->r, value);
	} else {
		auto found = next(node->l, value);
		return (found ? found : node);
	}
}

Node* AVLTree::prev(Node* node, int value) {
	if (!node)
		return nullptr;
	if (node->value >= value) {
		return prev(node->l, value);
	} else {
		auto found = prev(node->r, value);
		return (found ? found : node);
	}	
}

main() {
	std::ios::sync_with_stdio(false);
	cin.tie(0);
	std::string s = "";
	int value = 0;
	AVLTree tree = AVLTree();
	while (cin >> s) {
		cin >> value;
		if (s[0] == 'i') {
			tree.insert(value);
		} else if (s[0] == 'd') {
			tree.erase(value);
		} else if (s[0] == 'e') {
			cout << (tree.find(value)?"true":"false") << std::endl;
		} else if (s[0] == 'n') {
			auto found = tree.next(value);
			if (!found) {
				cout << "none" << std::endl;
			} else {
				cout << found->getValue() << std::endl;
			}	
		} else if (s[0] == 'p') {
			auto found = tree.prev(value);
			if (!found) {
				cout << "none" << std::endl;
			} else {
				cout << found->getValue() << std::endl;
			}
		}
	}
}
