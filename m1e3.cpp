#include <iostream>
#include <stack>
#include <vector>
	
/*
Идея - коротко:
Заметим, что любой максимальный искомый прямоугольник упирается высотой в какую-то гистограму.
Это значит что если для каждой гистограминки(рассмотрим i-ую)
можно посчитать наибольший прямоугольник, упирающийся в неё высотой. Высота такого прямоугольника = v[i].
Длина такого прямоугольника определяется ближайшим справа и ближайшим слева меньшим v[i].
*/

void findAnswerForInd(std::vector<int> &v, int ind, std::stack<int> &st, std::vector<int> &fill) {
	while (v[ind] <= v[st.top()]) {
		st.pop();
	}		
	fill[ind] = st.top();
	st.push(ind);
}

void clearStack(std::stack<int> &st){
	while (!st.empty()) {
		st.pop();
	}
}

long long maxRectangle(int n, std::vector<int> &v){
	std::vector<int> r(n + 2); // Первый справа меньший i-ого. Хранит индекс такого.
	std::vector<int> l(n + 2); // Первый слева меньший i-ого. Хранит индекс такого.
	std::stack<int> st; // Вспомогательный стэк	
	
	st.push(0);
	for (int i = 1; i <= n; ++i) { // Нахожу ближайших слева, меньших данного
		findAnswerForInd(v, i, st, l);
	}
	clearStack(st);

	st.push(n+1);
	for (int i = n; i >= 1; --i) { // Нахожу ближайших справа, меньших данного
		findAnswerForInd(v, i, st, r);
	}	
	
	long long ans = -1;
	for (int i = 1; i <= n; ++i) { // Ответ для i-ого = v[i] * (максимальную длину, где v[j] <= v[i]) = v[i] * (r[i] - l[i])
		ans = std::max(ans, 1LL * v[i] * (r[i] - l[i] - 1));
	}
	return ans;
}

int main(){
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0;
	std::cin >> n;
	std::vector<int> v(n + 2); //Значения массива
	for (int i = 1; i <= n; ++i) {
		std::cin >> v[i];
	}
	v[0] = (v[n + 1] = -1); //Создаю фиктивные элементы слева и справа, меньшие всех значений	
	std::cout << maxRectangle(n, v);
	return 0;	
}
