#include <iostream>
#include <vector>
#include <algorithm>

/*
Задача - вывести k наименьших элементов последовательности в порядке возрастания.
Решение - будем поддерживать кучу на максимум размера не более чем k из наименьших элементов.
Если куча маленькая(<k), то просто вставим очередной элемент Х
Если куча = k, то проверим, что максимальный > Х. Если так, то удалим максимальный и вставим Х.
Заметим, что так мы в куче будем поддерживать ровно k наименьших элементов.
В конце просто выведем их.
*/

struct Heap{
	int size;
	std::vector<int> nodes;
};

void initHeap(Heap* heap, int maxSize) {
	heap->nodes.resize(maxSize, 0);
	heap->size = 0;
}

void siftDown(Heap* heap, int vertex) {	
	while (2 * vertex <= heap->size) {
		int left = 2 * vertex;
		int right = 2 * vertex + 1;
		int where = -1;
		if (heap->nodes[left] > heap->nodes[vertex]) {
			where = left;
		}
		if (right <= heap->size && heap->nodes[right] > heap->nodes[vertex] && (where == -1 || heap->nodes[right] > heap->nodes[left])) {
			where = right;
		}
		if (where == -1) {
			break;
		}
		std::swap(heap->nodes[vertex], heap->nodes[where]);
		vertex = where;
	}
}
 
void siftUp(Heap* heap, int vertex) {
	int parent = vertex / 2;
	while (parent > 0 && heap->nodes[vertex] > heap->nodes[parent]) {
		std::swap(heap->nodes[vertex], heap->nodes[vertex / 2]);
		vertex = parent;
		parent /= 2;
	}
}

void insert(Heap* heap, int val) {
	heap->nodes[++heap->size] = val;
	siftUp(heap, heap->size);
}

int getMax(Heap* heap) {
	return heap->nodes[1];
}

void eraseMax(Heap* heap) {
	std::swap(heap->nodes[1], heap->nodes[heap->size]);
	heap->nodes[heap->size--] = 0;
	if (heap->size > 0) {
		siftDown(heap, 1);
	}
}

int main() {
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0, k = 0;
	std::cin >> n >> k;
	Heap heap = Heap();
	initHeap(&heap, k + 1);
	for (int i = 0; i < n; ++i) {
		int num = 0;
		std::cin >> num;
		if (heap.size < k) {
			insert(&heap, num);
		} else if (getMax(&heap) > num) {
			eraseMax(&heap);
			insert(&heap, num);
		}
	}
	std::vector<int> ans(k, 0);
	for (int i = 0; i < k; ++i) {
		ans[i] = getMax(&heap);
		eraseMax(&heap);
	}
	std::reverse(ans.begin(), ans.end());
	for (auto t : ans) {
		std::cout << t << " ";
	}
}
