#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

/*
Задача:
Требуется найти в связном графе остовное дерево минимального веса.

Воспользуйтесь алгоритмом Прима.

Входные данные
Первая строка входного файла содержит два натуральных числа n и m —
количество вершин и ребер графа соответственно.
Следующие m строк содержат описание ребер по одному на строке.
Ребро номер i описывается тремя натуральными числами bi, ei и wi —
номера концов ребра и его вес соответственно (1≤bi,ei≤n, 0≤wi≤100000). n≤5000,m≤100000.
Граф является связным.

Выходные данные
Первая строка выходного файла должна содержать одно натуральное число —
вес минимального остовного дерева.

Решение:
Стандартная реализация.
*/

struct Edge {
    int begin;
    int end;
    int weight;
    Edge reversed() const {
        Edge ret = *this;
        std::swap(ret.begin, ret.end);
        return ret;
    }
};

class Graph{
public:
    explicit Graph(int n): edges(n, std::vector<Edge>()) {}
    void addEdge(const Edge& e) {
        edges[e.begin].push_back(e);
        edges[e.end].push_back(e.reversed());
    } 
    const std::vector<Edge>& getNeighbours(int v) const {
        return edges[v];
    }
    int size() const {
        return edges.size();
    }
private:
    std::vector<std::vector<Edge>> edges; 
};

int findMST(const Graph& g) {
    const int INF = 1e9;
    std::vector<int> min_e(g.size(), INF);
    std::vector<bool> used(g.size(), 0);
    std::set<std::pair<int,int>> notMST;
    min_e[0] = 0;
    notMST.insert({min_e[0], 0});
    int ans = 0;
    while (!notMST.empty()) {
        int v = notMST.begin()->second;
        ans += notMST.begin()->first;
        notMST.erase(notMST.begin());
        used[v] = 1;
        for (auto &edge: g.getNeighbours(v)) {
            int to = edge.end;
            int w = edge.weight;
            if (w < min_e[to] && !used[to]) {
                notMST.erase({min_e[to], to});
                min_e[to] = w;
                notMST.insert({min_e[to], to});
            }
        }
    }
    return ans;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0, m = 0;
    std::cin >> n >> m;
    Graph g(n);
    for (int i = 0; i < m; i++) {
        Edge e;
        std::cin >> e.begin >> e.end >> e.weight;
        e.begin--, e.end--;
        g.addEdge(e);
    }
    std::cout << findMST(g);
}
