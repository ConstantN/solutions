#include <iostream>
#include <vector>

template <size_t blockSize>
class FixedAllocator {
public:

    void* allocate(size_t) {
        if (blocks.size() == 0) {
            int8_t* mem = new int8_t[blockSize * blockNumber];
            heads.push_back(mem);
            for (int i = 0; i < blockNumber; ++i) {
                blocks.push_back(reinterpret_cast<void*>(mem + i * blockSize));
            }
        }
        auto t = blocks.back();
        blocks.pop_back();
        return t;
    }

    void deallocate(void* ptr, size_t) {
        blocks.push_back(ptr);
    }

    ~FixedAllocator() {
        for (size_t i = 0; i < heads.size(); ++i) {
            delete[] heads[i];
        }
    }

private:
    const int blockNumber = 1000;
    std::vector<void*> blocks;
    std::vector<int8_t*> heads;
};

const int int32_bucket = 24;
FixedAllocator<int32_bucket> int32_alloc;
const int int64_bucket= 32;
FixedAllocator<int64_bucket> int64_alloc;

template <typename T>
class FastAllocator {
public:
    using value_type = T;

    FastAllocator() {}

    template<typename U>
    FastAllocator(const FastAllocator<U>&) {}

    template<typename U>
    FastAllocator& operator=(const FastAllocator<U>&) {
        return *this;
    }

    T* allocate(size_t n) {
        if (sizeof(T) == int32_bucket && n == 1) {
            return reinterpret_cast<T*>(int32_alloc.allocate(n));
        } else if (sizeof(T) == int64_bucket && n == 1) {
            return reinterpret_cast<T*>(int64_alloc.allocate(n));
        } else {
            return reinterpret_cast<T*>(::operator new(n * sizeof(T)));
        }
    }

    void deallocate(T* ptr, size_t n) {
        if (sizeof(T) == int32_bucket && n == 1) {
            int32_alloc.deallocate(ptr, n);
        } else if (sizeof(T) == int64_bucket && n == 1) {
            int64_alloc.deallocate(ptr, n);
        } else {
            ::operator delete(ptr);
        }
    }
};

template <typename T1, typename T2>
bool operator==(const FastAllocator<T1>&, const FastAllocator<T2>&) {
    return true;
}

template <typename T1, typename T2>
bool operator!=(const FastAllocator<T1>&, const FastAllocator<T2>&) {
    return false;
}

template <typename T, typename AllocType = std::allocator<T>>
class List {
private:
    template<bool IsConst>
    struct common_iterator;
    struct Node;
    using NodeAllocType = typename std::allocator_traits<AllocType>::template rebind_alloc<Node>;
    using AllocTraits = std::allocator_traits<NodeAllocType>;
public:
    using iterator = common_iterator<false>;
    using const_iterator = common_iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;


    explicit List(const AllocType& allocator = AllocType()): allocator_(allocator) {
        end_ = AllocTraits::allocate(allocator_, 1);
        end_->prev = end_->next = end_;
        size_ = 0;
    }

    List(size_t count, const T& value, const AllocType& allocator = AllocType()): List(allocator) {
        for (size_t i = 0; i < count; ++i) {
            push_back(value);
        }
    }

    explicit List(size_t count, const AllocType& allocator = AllocType()): List(allocator) {
        for (size_t i = 0; i < count; ++i) {
            auto iter = --end();
            ++size_;
            Node* new_node = AllocTraits::allocate(allocator_, 1);
            AllocTraits::construct(allocator_, new_node);
            Node* node_next = const_cast<Node*>(iter.node);
            Node* node_prev = node_next->prev;
            new_node->prev = node_prev;
            new_node->next = node_next;
            node_prev->next = node_next->prev = new_node;
        }
    }

    List(const List& list) {
        allocator_ = AllocTraits::select_on_container_copy_construction(list.allocator_);
        end_ = AllocTraits::allocate(allocator_, 1);
        end_->prev = end_->next = end_;
        size_ = 0;
        for (auto t = list.begin(); t != list.end(); ++t) {
            push_back(*t);
        }
    }

    ~List() {
        while(size() > 0) {
            pop_back();
        }
        AllocTraits::deallocate(allocator_, end_, 1);
    }

    List& operator=(const List& list) {
        if (AllocTraits::propagate_on_container_copy_assignment::value && allocator_ != list.allocator_) {
            allocator_ = list.allocator_;
        }
        while (size() > list.size()) {
            pop_back();
        }
        auto j = list.begin();
        for (auto i = begin(); i != end() && j != list.end(); ++i, ++j) {
            (*i) = (*j);
        }
        for (; j != list.end(); ++j) {
            push_back(*j);
        }
        return *this;
    }

    AllocType get_allocator() {
        return allocator_;
    }

    size_t size() const {
        return size_;
    }

    void push_back(const T& value) {
        insert(end(), value);
    }

    void push_front(const T& value) {
        insert(begin(), value);
    }

    void pop_back() {
        erase(--end());
    }

    void pop_front() {
        erase(begin());
    }

    void insert(const_iterator iter, const T& value) { 
        Node* new_node = AllocTraits::allocate(allocator_, 1);
        AllocTraits::construct(allocator_, new_node, value);
        Node* node_next = const_cast<Node*>(iter.node);
        Node* node_prev = node_next->prev;
        new_node->prev = node_prev;
        new_node->next = node_next;
        node_prev->next = node_next->prev = new_node;
        ++size_;
    }

    void erase(const_iterator iter) { 
        Node* node = const_cast<Node*>(iter.node);
        Node* node_next = node->next;
        Node* node_prev = node->prev;
        node_next->prev = node_prev;
        node_prev->next = node_next;
        AllocTraits::destroy(allocator_, node);
        AllocTraits::deallocate(allocator_, node, 1);
        --size_;
    }

    iterator begin() {
        return iterator(end_->next);
    }

    iterator end() {
        return iterator(end_);
    }

    const_iterator begin() const {
        return const_iterator(end_->next);
    }

    const_iterator end() const {
        return const_iterator(end_);
    }

    const_iterator cbegin() const {
        return const_iterator(end_->next);
    }

    const_iterator cend() const {
        return const_iterator(end_);
    }

    reverse_iterator rbegin() {
        return std::make_reverse_iterator(end());
    }

    reverse_iterator rend() {
        return std::make_reverse_iterator(begin());
    }

    const_reverse_iterator rbegin() const {
        return std::make_reverse_iterator(cend());
    }

    const_reverse_iterator rend() const {
        return std::make_reverse_iterator(cbegin());
    }

    const_reverse_iterator crbegin() const {
        return std::make_reverse_iterator(cend());
    }

    const_reverse_iterator crend() const {
        return std::make_reverse_iterator(cbegin());
    }

private:
    Node* end_;
    size_t size_;
    NodeAllocType allocator_;

    struct Node {
        Node(): value() {
            next = prev = nullptr;
        }
        Node(const T& value): value(value) {
            next = prev = nullptr;
        }
        T value;
        Node* next;
        Node* prev;
    };

    template<bool IsConst>
    struct common_iterator {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = typename std::conditional<IsConst, const T, T>::type;
        using pointer = typename std::conditional<IsConst, const T*, T*>::type;
        using reference = typename std::conditional<IsConst, const T&, T&>::type;
        using iterator_category = std::bidirectional_iterator_tag;
        common_iterator(Node* node): node(node) {}
        reference operator*() const {
            return node->value;
        }
        pointer operator->() const {
            return &node->value;
        }
        common_iterator& operator++() {
            node = node->next;
            return *this;
        }
        common_iterator operator++(int) {
            common_iterator ret = *this;
            ++(*this);
            return ret;
        }
        common_iterator& operator--() {
            node = node->prev;
            return *this;
        }
        common_iterator operator--(int) {
            common_iterator ret = *this;
            --(*this);
            return ret;
        }
        bool operator==(const common_iterator& iter) const {
            return node == iter.node;
        }
        bool operator!=(const common_iterator& iter) const {
            return !(*this == iter);
        }
        operator common_iterator<true>() const {
            return common_iterator<true>(node);
        }
        friend common_iterator<true>;
        friend common_iterator<false>;
        friend List;
    private:
        Node* node;
    };
};
