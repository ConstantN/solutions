#include <iostream>
#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <typeinfo>
#include <cassert>
#include <algorithm>

class Point;
class Line;
class Polygon;
class Rectangle;
class Ellipse;
class Circle;

bool isEqual(double a, double b) {
	const double eps = 1e-5;
	return (std::abs(a - b) < eps);
}

struct Point {
	double x;
	double y;
	Point(double, double);
	bool operator==(const Point&) const;
	bool operator!=(const Point&) const;
	Point operator-(const Point&) const;
	Point operator+(const Point&) const;
	Point operator*(double) const;
	double distanceTo(const Point&) const;
	double angleCos(const Point&) const;
	double angleSin(const Point&) const;
	void rotate(const Point&, double);
	void reflex(const Point&);
	void reflex(const Line&);
	void scale(const Point&, double);
	Point project(const Line&) const;
	double length() const;
};

class Line {
public:
	Line(const Point&, const Point&);
	Line(double, double);
	Line(const Point&, double);
	bool operator==(const Line&) const;
	bool operator!=(const Line&) const;
	Point getFirst() const;
	Point getSecond() const;
	Point intersect(const Line&) const;
	Line createNormal(const Point&) const;
private:
	Point first;
	Point second;
	bool belongs(const Point&) const;
};

class Shape {
public:
	double virtual perimeter() const = 0;
	double virtual area() const = 0;
	bool virtual operator==(const Shape&) const = 0;
	bool operator!=(const Shape&) const;
	bool virtual isCongruentTo(const Shape&) const = 0;
	bool virtual isSimilarTo(const Shape&) const = 0;
	bool virtual containsPoint(const Point&) const = 0;
	void virtual rotate(const Point&, double) = 0;
	void virtual reflex(const Point&) = 0;
	void virtual reflex(const Line&) = 0;
	void virtual scale(const Point&, double) = 0;
	virtual ~Shape() = 0;
};

Shape::~Shape(){}

bool Shape::operator!=(const Shape& shape) const {
	return !(*this == shape);
}

class Polygon: public Shape {
public:
	Polygon() = default;
	Polygon(const std::vector<Point>&);
	Polygon(const std::initializer_list<Point>&);
	size_t verticesCount() const;
	const std::vector<Point>& getVertices() const;
	bool isConvex() const;
	double perimeter() const override;
	double area() const override;
	bool operator==(const Shape&) const override;
	bool isCongruentTo(const Shape&) const override;
	bool isSimilarTo(const Shape&) const override;
	bool containsPoint(const Point&) const override;
	void rotate(const Point&, double) override;
	void reflex(const Point&) override;
	void reflex(const Line&) override;
	void scale(const Point&, double) override;
protected:
	std::vector<Point> vertices;
	std::vector<std::vector<double>> getValues() const;
	std::vector<std::vector<double>> getReverseValues() const;
};

class Rectangle: public Polygon {
public:
	Rectangle(const Point&, const Point&, double);
	Point center() const;
	std::pair<Line, Line> diagonals() const;
};

class Square: public Rectangle {
public:
	Square(const Point&, const Point&);
	Circle circumscribedCircle() const;
	Circle inscribedCircle() const;
};

class Triangle: public Polygon {
public:
	Triangle(const Point&, const Point&, const Point&);
	Point centroid() const;
	Point orthocenter() const;
	Line EulerLine() const;
	Circle circumscribedCircle() const;
	Circle inscribedCircle() const;
	Circle ninePointsCircle() const;
};

class Ellipse: public Shape {
public:
	Ellipse(const Point&, const Point&, double);
	std::pair<Point, Point> focuses() const;
	std::pair<Line, Line> directrices() const;
	double eccentricity() const;
	Point center() const;
	double focus() const;
	double getA() const;
	double getB() const;
	double perimeter() const override;
	double area() const override;
	bool operator==(const Shape&) const override;
	bool isCongruentTo(const Shape&) const override;
	bool isSimilarTo(const Shape&) const override;
	bool containsPoint(const Point&) const override;
	void rotate(const Point&, double) override;
	void reflex(const Point&) override;
	void reflex(const Line&) override;
	void scale(const Point&, double) override;
protected:
	Point f1;
	Point f2;
	double r;
};

class Circle: public Ellipse {
public:
	Circle(const Point&, double);
	double radius() const;
};

Point::Point(double x, double y): x(x), y(y) {}

bool Point::operator==(const Point& p) const {
	return (isEqual(x, p.x) && isEqual(y, p.y));
}

bool Point::operator!=(const Point& p) const {
	return !(*this == p);
}

Point Point::operator-(const Point& p) const {
	return Point(x - p.x, y - p.y);
}

Point Point::operator+(const Point& p) const {
	return Point(x + p.x, y + p.y);
}

Point Point::operator*(double coefficient) const {
	return Point(x * coefficient, y * coefficient);
}

double Point::distanceTo(const Point& p) const {
	return (*this - p).length();
}

double Point::angleCos(const Point& p) const {
	return (x * p.x + y * p.y) / length() / p.length();
}

double Point::angleSin(const Point& p) const {
	return (x * p.y - y * p.x) / length() / p.length();
}

void Point::rotate(const Point& center, double angle) {
	angle = angle / 180 * M_PI;
	double X(center.x + (x - center.x) * std::cos(angle) - (y - center.y) * std::sin(angle));
	double Y(center.y + (y - center.y) * std::cos(angle) + (x - center.x) * std::sin(angle));
	x = X, y = Y;
}

void Point::reflex(const Point& center) {
	*this = center + (center - *this);
} 

void Point::reflex(const Line& axis) {
	Point pr = (*this).project(axis);
	reflex(pr);
}

void Point::scale(const Point& center, double coefficient) {
	Point v = *this - center;
	*this = center + v * double(coefficient); 
}

Point Point::project(const Line& axis) const {
	Point v = axis.getSecond() - axis.getFirst();
	Point side = (*this) - axis.getFirst();
	double projectionLength = (side.x * v.x + side.y * v.y) / v.length();
	return axis.getFirst() + v * (projectionLength / v.length());
}

double Point::length() const {
	return std::sqrt(x * x + y * y);
} 

Line::Line(const Point& first, const Point& second): first(first), second(second) {}

Line::Line(double k, double b): first(1, k + b), second(-1, -k + b) {}

Line::Line(const Point& point, double k): first(point), second(point.x + 1, k * (point.x + 1) + (point.y - k * point.x)) {}

bool Line::operator==(const Line& line) const {
	return belongs(line.first) && belongs(line.second);
}

bool Line::operator!=(const Line& line) const {
	return !(*this == line);
}

Point Line::getFirst() const {
	return first;
}

Point Line::getSecond() const {
	return second;
}

Point Line::intersect(const Line& line) const {
	Point v = line.getSecond() - line.getFirst();
	double k = ((line.getFirst().y - first.y) * (second.x - first.x) - (line.getFirst().x - first.x) * (second.y - first.y)) / (v.x * (second.y - first.y) - v.y * (second.x - first.x));
	return line.getFirst() + v * k;
}

Line Line::createNormal(const Point& p) const {
	Point v = second - first;
	std::swap(v.x, v.y);
	v.x = -v.x;
	Point p1 = p + v;
	return Line(p, p1);
}

bool Line::belongs(const Point& p) const {
	return isEqual((p.x - first.x)*(second.y - first.y), (p.y - first.y)*(second.x - first.x));
}

Polygon::Polygon(const std::vector<Point>& vertices): vertices(vertices) {
}

Polygon::Polygon(const std::initializer_list<Point>& lst): vertices(lst) {
}

size_t Polygon::verticesCount() const {
	return vertices.size();
} 

const std::vector<Point>& Polygon::getVertices() const {
	return vertices;
}

bool Polygon::isConvex() const {
	double prev(0);
	size_t size = vertices.size();
	for (size_t i = 0; i < size; ++i) {
		Point v = vertices[i];
		Point v1 = vertices[(i + 1) % size];
		Point v2 = vertices[(i + 2) % size];
		double sin = (v1 - v).angleSin(v2 - v1);
		if (sin * prev > 0.0 || isEqual(sin * prev, 0.0)) 
			prev = sin;
		else
			return false;
	}
	return true;
}

double Polygon::perimeter() const {
	double perimeter(0);
	size_t size = vertices.size();
	for (size_t i = 0; i < size; ++i) {
		size_t next = (i + 1) % size;
		perimeter += vertices[i].distanceTo(vertices[next]);
	}
	return perimeter;
}

double Polygon::area() const {
	double area(0);
	size_t size = vertices.size();
	for (size_t i = 0; i < size; ++i) {
		size_t next = (i + 1) % size;
		area += (vertices[next].x - vertices[i].x) * (vertices[next].y + vertices[i].y) / 2.0;
	}
	return std::abs(area);
}

bool Polygon::operator==(const Shape& shape) const {
	try {
		const Polygon& shapePolygon = dynamic_cast<const Polygon&>(shape);
		if (vertices.size() != shapePolygon.vertices.size())
			return false;
		size_t indexStart = 0;
		for (; indexStart < vertices.size(); ++indexStart) {
			if (shapePolygon.vertices[indexStart] == vertices[0])
				break;
		}
		if (indexStart == vertices.size())
			return false;
		int found = 2;
		for (size_t attempt = 0; attempt < 2; ++attempt) {
			for (size_t i = 0; i < vertices.size(); ++i) {
				size_t ii = i;
				if (attempt)
					ii = (vertices.size() - i) % vertices.size();
				if (vertices[ii] != shapePolygon.vertices[(indexStart + i) % shapePolygon.vertices.size()]){
					--found;
					break;	
				}
			}
		}
		return found;
	} catch (const std::bad_cast&) {
		return false;
	}
}

bool Polygon::isCongruentTo(const Shape& shape) const {
	try {
		const Polygon& shapePolygon = dynamic_cast<const Polygon&>(shape);
		if (vertices.size() != shapePolygon.vertices.size())
			return false;
		
		std::vector<std::vector<double>> values;
		std::vector<std::vector<double>> shapeValues;
		for (int coef = -1; coef <= 1; coef += 2) {
			for (size_t attempt = 0; attempt < 2; ++attempt) {
				if (!attempt) {
					values = getValues();
					shapeValues = shapePolygon.getValues();
				} else {
					values = getValues();
					shapeValues = shapePolygon.getReverseValues();
				}
				for (int i = 0; i < (int)vertices.size(); ++i)
						shapeValues[i][2] *= double(coef);
				for (size_t indexStart = 0; indexStart < vertices.size(); ++indexStart) {
					bool ok = true;
					for (size_t i = 0; i < vertices.size(); ++i) {
						size_t shapeIndex = (indexStart + i) % shapePolygon.vertices.size();
						if (!isEqual(values[i][0], shapeValues[shapeIndex][0]) || !isEqual(values[i][1], shapeValues[shapeIndex][1]) || !isEqual(values[i][2], shapeValues[shapeIndex][2])) {
							ok = false;
							break;	
						}
					}
					if (ok) {
						return true;	
					}
				}
			}	
		}
		return false;
	} catch (const std::bad_cast&) {
		return false;
	}
	return false;
}

bool Polygon::isSimilarTo(const Shape& shape) const {
	try {
		const Polygon& shapePolygon = dynamic_cast<const Polygon&>(shape);
		if (vertices.size() != shapePolygon.vertices.size())
			return false;		
		std::vector<std::vector<double>> values;
		std::vector<std::vector<double>> shapeValues;
		
		for (int coef = -1; coef <= 1; coef += 2) {
			for (size_t attempt = 0; attempt < 2; ++attempt) {
				if (!attempt) {
					values = getValues();
					shapeValues = shapePolygon.getValues();
				} else {
					values = getValues();
					shapeValues = shapePolygon.getReverseValues();
				}
				for (int i = 0; i < (int)vertices.size(); ++i)
					shapeValues[i][2] *= double(coef);
				for (size_t indexStart = 0; indexStart < vertices.size(); ++indexStart) {
					bool ok = true;
					for (size_t i = 0; i < vertices.size(); ++i) {
						size_t shapeIndex = (indexStart + i) % shapePolygon.vertices.size();
						if (!isEqual(values[i][1], shapeValues[shapeIndex][1]) || !isEqual(values[i][2], shapeValues[shapeIndex][2]) || !isEqual(values[i][0] / values[i][3], shapeValues[shapeIndex][0] / shapeValues[shapeIndex][3])) {
							ok = false;
							break;	
						}
					}
					if (ok) {
						return true;
					}
				}
			}	
		}
		return false;
	} catch (const std::bad_cast&) {
		return false;
	}
}

bool Polygon::containsPoint(const Point& p) const {
	size_t size = vertices.size();
	double angle(0);
	for (size_t i = 0; i < vertices.size(); ++i) {
		Point v1 = vertices[i] - p;
		Point v2 = vertices[(i + 1) % size] - p;
		double Angle(0);
		if (v1.angleCos(v2) > 0) {
			Angle = std::asin(v1.angleSin(v2));
		} else {
			if (v1.angleSin(v2) > 0) {
				Angle = double(M_PI) - std::asin(v1.angleSin(v2));
			} else {
				Angle = - double(M_PI) - std::asin(v1.angleSin(v2));
			}
		}
		angle += Angle;
	}
	return isEqual(std::abs(angle), 2 * M_PI);
}

void Polygon::rotate(const Point& center, double angle) {
	for (auto &point : vertices) {
		point.rotate(center, angle);
	}
}

void Polygon::reflex(const Point& center) {
	for (auto &point : vertices) {
		point.reflex(center);
	}
}

void Polygon::reflex(const Line& axis) {
	for (auto &point : vertices) {
		point.reflex(axis);
	}
}

void Polygon::scale(const Point& center, double coefficient) {
	for (auto &point : vertices) {
		point.scale(center, coefficient);
	}
}

std::vector<std::vector<double>> Polygon::getValues() const {
	std::vector<std::vector<double>> res(vertices.size(), std::vector<double>());
	for (size_t i = 0; i < vertices.size(); ++i) {
		Point A = vertices[i];
		Point B = vertices[(vertices.size() + i - 1) % vertices.size()];
		Point C = vertices[(i + 1) % vertices.size()];
		res[i] = {A.distanceTo(B), (B - A).angleCos(C - A), (B - A).angleSin(C - A), A.distanceTo(C)};
	}
	return res;
}

std::vector<std::vector<double>> Polygon::getReverseValues() const {
	std::vector<std::vector<double>> res(vertices.size(), std::vector<double>());
	for (size_t i = 0; i < vertices.size(); ++i) {
		Point A = vertices[i];
		Point C = vertices[(vertices.size() + i - 1) % vertices.size()];
		Point B = vertices[(i + 1) % vertices.size()];
		res[i] = {A.distanceTo(B), (B - A).angleCos(C - A), (B - A).angleSin(C - A), A.distanceTo(C)};
	}
	std::reverse(res.begin(), res.end());
	return res;
}

Rectangle::Rectangle(const Point& p1, const Point& p2, double ratio) {
	if (ratio > 1)
		ratio = 1 / ratio;
	Point center = (p1 + p2) * 0.5;
	Point p4 = p1;
	p4.rotate(center, 360 - 180 / M_PI * 2 * std::atan(ratio));
	Point p3 = p4;
	p3.reflex(center);
	vertices = {p1, p3, p2, p4};
}

Point Rectangle::center() const {
	return (vertices[0] + vertices[2]) * 0.5;
}

std::pair<Line, Line> Rectangle::diagonals() const {
	return {Line(vertices[0], vertices[2]), Line(vertices[1], vertices[3])};
}

Square::Square(const Point& p1, const Point& p2): Rectangle(p1, p2, 1) {}

Circle Square::circumscribedCircle() const {
	return Circle((vertices[0] + vertices[2]) * 0.5, vertices[0].distanceTo(vertices[2]) * 0.5);
}

Circle Square::inscribedCircle() const {
	return Circle((vertices[0] + vertices[2]) * 0.5, vertices[0].distanceTo(vertices[1]) * 0.5);
}

Line makeBisector(Point A, Point B, Point C) {
	double AB = A.distanceTo(B);
	double AC = A.distanceTo(C);
	double kB = AC / (AB + AC);
	double kC = AB / (AB + AC);
	Point A1 = B * kB + C * kC;
	return Line(A, A1);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c): Polygon({a, b, c}) {
}

Point Triangle::centroid() const {
	return (vertices[0] + vertices[1] + vertices[2]) * double(1.0/3);
}

Point Triangle::orthocenter() const {
	Point a = vertices[0];
	Point b = vertices[1];
	Point c = vertices[2];	
	Point ap = a.project(Line(b,c));
	Point bp = b.project(Line(a,c));
	return Line(a, ap).intersect(Line(b, bp));
}

Line Triangle::EulerLine() const {
	return Line(orthocenter(), centroid());
}

Circle Triangle::circumscribedCircle() const {
	Point mid01 = (vertices[0] + vertices[1]) * 0.5;
	Line side01 = Line(vertices[0], vertices[1]);
	Point mid12 = (vertices[1] + vertices[2]) * 0.5;
	Line side12 = Line(vertices[1], vertices[2]);
	Point center = side01.createNormal(mid01).intersect(side12.createNormal(mid12));
	return Circle(center, center.distanceTo(vertices[0]));
}

Circle Triangle::inscribedCircle() const {
	Line A = makeBisector(vertices[0], vertices[1], vertices[2]);
	Line B = makeBisector(vertices[1], vertices[0], vertices[2]);
	Point center = A.intersect(B);
	Point projection = center.project(Line(vertices[0], vertices[1]));
	return Circle(center, center.distanceTo(projection));
}

Circle Triangle::ninePointsCircle() const {
	Point center = (orthocenter() + circumscribedCircle().center()) * 0.5;
	Point A1 = vertices[0].project(Line(vertices[1], vertices[2]));
	return Circle(center, center.distanceTo(A1));
} 

Ellipse::Ellipse(const Point& f1, const Point& f2, double r): f1(f1), f2(f2), r(r) {
}

std::pair<Point, Point> Ellipse::focuses() const {
	return {f1, f2};
}

std::pair<Line, Line> Ellipse::directrices() const {
	Line focusLine(f1, f2);
	Point v = f1 - f2;
	v = v * (getA() / eccentricity() / v.length());
	Point A1 = center() + v;
	Point A2 = center() - v;
	return {focusLine.createNormal(A1), focusLine.createNormal(A2)};
} 

double Ellipse::eccentricity() const {
	return focus() / getA();	
}

Point Ellipse::center() const {
	return (f1 + f2) * 0.5;
}

double Ellipse::focus() const {
	return f1.distanceTo(f2) * 0.5;
}

double Ellipse::getA() const {
	return r / 2.0;
}

double Ellipse::getB() const {
	return std::sqrt((r / 2.0) * (r / 2.0) - focus() * focus());
} 

double Ellipse::perimeter() const {
	double a = getA();
	double b = getB();
	return double(M_PI) * (3.0 * (a + b) - std::sqrt((3.0 * a + b) * (3.0 * b + a)));
}

double Ellipse::area() const {
	return double(M_PI) * getA() * getB();
}

bool Ellipse::operator==(const Shape& shape) const {
	try {
		const Ellipse& shapeEllipse = dynamic_cast<const Ellipse&>(shape);
		return ((f1 == shapeEllipse.f1 && f2 == shapeEllipse.f2) || (f1 == shapeEllipse.f2 && f2 == shapeEllipse.f1)) && isEqual(r, shapeEllipse.r);
	} catch (const std::bad_cast&) {
		return false;
	}
}

bool Ellipse::isCongruentTo(const Shape& shape) const {
	try {
		const Ellipse& shapeEllipse = dynamic_cast<const Ellipse&>(shape);
		return isEqual(f1.distanceTo(f2), shapeEllipse.f1.distanceTo(shapeEllipse.f2)) && isEqual(r, shapeEllipse.r);
	} catch (const std::bad_cast&) {
		return false;
	}
}

bool Ellipse::isSimilarTo(const Shape& shape) const {
	try {
		const Ellipse& shapeEllipse = dynamic_cast<const Ellipse&>(shape);
		return isEqual(focus() / shapeEllipse.focus(), r / shapeEllipse.r);
	} catch (const std::bad_cast&) {
		return false;
	}
}

bool Ellipse::containsPoint(const Point& point) const {
	return point.distanceTo(f1) + point.distanceTo(f2) < r;
}

void Ellipse::rotate(const Point& center, double angle) {
	f1.rotate(center, angle);
	f2.rotate(center, angle);
}

void Ellipse::reflex(const Point& center) {
	f1.reflex(center);
	f2.reflex(center);
}

void Ellipse::reflex(const Line& axis) {
	f1.reflex(axis);
	f2.reflex(axis);
}

void Ellipse::scale(const Point& center, double coefficient) {
	f1.scale(center, coefficient);
	f2.scale(center, coefficient);
	r *= double(coefficient);
}

Circle::Circle(const Point& center, double radius): Ellipse(center, center, 2 * radius) {}

double Circle::radius() const {
	return r * 0.5;
}
