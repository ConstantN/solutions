#include <bits/stdc++.h>

using namespace std;

void outputPrimes(int n){ //in assuming n>0
	bool f[n+1] = {};
	f[0] = (f[1] = 1);
	for(int i = 0; i <= n; i++){
		if(f[i] == 0){
			cout<<i<<" ";
			for(int k = i*i; k <= n; k+=i){
				f[k] = 1;
			}
		}
	}
}

int main(){
	int n;
	cin>>n;
	outputPrimes(n);
}
