#include <iostream>
#include <vector>

/*
Требуется построить в двудольном графе минимальное контролирующее множество,
если дано максимальное паросочетание.

Входные данные
В первой строке файла даны два числа m и n (1≤m,n≤4000) — размеры долей.
Каждая из следующих m строк содержит список ребер, выходящих из соответствующей вершины первой доли.
Этот список начинается с числа Ki (0≤Ki≤n) — количества ребер,
после которого записаны вершины второй доли, соединенные с данной вершиной первой доли,
в произвольном порядке. Сумма всех Ki во входном файле не превосходит 500000.
Последняя строка файла содержит некоторое максимальное паросочетание в этом графе —
m чисел 0≤Li≤n — соответствующая i-й вершине первой доли вершина второй доли, или 0,
если i-я вершина первой доли не входит в паросочетание.

Выходные данные
Первая строка содержит размер минимального контролирующего множества.
Вторая строка содержит количество вершин первой доли S, после которого записаны S чисел —
номера вершин первой доли, входящих в контролирующее множество, в возрастающем порядке.
Третья строка содержит описание вершин второй доли в аналогичном формате.

Решение:
Стандартная реализация.
*/

class BipartiteGraph {
public:
    explicit BipartiteGraph(int LSize, int RSize): L(LSize, std::vector<int>()), R(RSize, std::vector<int>()) {}
    int getLSize() const {
        return L.size();
    }
    int getRSize() const {
        return R.size();
    }
    const std::vector<int> getLNeighbours(int v) const {
        return L[v];
    }
    const std::vector<int> getRNeighbours(int v) const {
        return R[v];
    }
    void addEdge(int l, int r) {
        L[l].push_back(r);
        R[r].push_back(l);
    }
private:
    std::vector<std::vector<int>> L;
    std::vector<std::vector<int>> R;
};

class BiGraphProcessor {
public:
    explicit BiGraphProcessor(const BipartiteGraph& g, const std::vector<int>& matchL): 
            graph(g), usedL(g.getLSize(), false), usedR(g.getRSize(), false), matchL(matchL) {
        matchR.resize(g.getRSize(), -1);
        for (int i = 0; i < matchL.size(); i++) {
            if (matchL[i] != -1) {
                matchR[matchL[i]] = i;
            }
        }
    }
    void findMCS(std::vector<int>& Lans, std::vector<int>& Rans) {
        for (int i = 0; i < matchL.size(); i++) {
            if (matchL[i] == -1 && !usedL[i]) {
                dfs(i, 1);
            }
        }
        for (int i = 0; i < usedL.size(); i++) {
            if (!usedL[i]) {
                Lans.push_back(i);
            }
        }
        for (int i = 0; i < usedR.size(); i++) {
            if (usedR[i]) {
                Rans.push_back(i);
            }
        }
    }
private:
    const BipartiteGraph& graph;
    std::vector<bool> usedL;
    std::vector<bool> usedR;
    std::vector<int> matchL;
    std::vector<int> matchR; 
    void dfs(int v, bool isL) {
        if (!isL) {
            usedR[v] = 1;
            if (matchR[v] != -1 && !usedL[matchR[v]]) {
                dfs(matchR[v], 1);
            } 
            return;
        }
        usedL[v] = 1;
        for (auto t: graph.getLNeighbours(v)) {
            if (t != matchL[v] && !usedR[t]) {
                dfs(t, 0);
            } 
        }
    }
};


int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int m = 0, n = 0;
    std::cin >> m >> n;
    BipartiteGraph graph(m, n);
    for (int i = 0; i < m; i++) {
        int size = 0;
        std::cin >> size;
        for (int j = 0; j < size; j++) {
            int c = 0;
            std::cin >> c;
            graph.addEdge(i, c - 1);
        }
    }
    std::vector<int> match(m);
    for (auto &t : match) {
        std::cin >> t;
        t--;
    }
    std::vector<int> Lans, Rans;
    BiGraphProcessor(graph, match).findMCS(Lans, Rans);
    std::cout << Lans.size() + Rans.size() << "\n";
    std::cout << Lans.size() << " ";
    for (auto t : Lans) {
        std::cout << t + 1 << " ";
    }
    std::cout << "\n" << Rans.size() << " ";
    for (auto t : Rans) {
        std::cout << t + 1 << " ";
    }
}
