#include <iostream>

using std::cin;
using std::cout;

/*
Задача - дан массив из n чисел.
Назовём ударом - вычитание из одного числа p, а из всех остальных q, где p > q
Перефразируем удар - вычитание из всех чисел q, а из одного ещё p-q;
За какое минимальное число ударов можно сделать все числа массива неположительными?

Решение - бинарный поиск по ответу.
*/

//Функция couldBeSolved отвечает на вопрос - можно ли за hits ударов решить задачу.
bool couldBeSolved(int hits, int* health, int n, int oneHit, int everyHit){
	long long hitsNeed = 0;
	for (int i = 0; i < n; ++i) {
		int remain = std::max(0ll, health[i] - 1ll * hits * everyHit); 
		if (oneHit == 0 && remain > 0) { //Не сможем
			hitsNeed += 1e9 + 1; //+бесконечность
		} else if(oneHit != 0) {
			hitsNeed += remain / oneHit + (remain % oneHit != 0);
		}
	}
	return hitsNeed <= hits;
}

//Функция binSearch решает задачу
int binSearch(int* a, int n, int p, int q){
	int L = 0; //За 0 ударов точно нельзя убить всех монстров, т.к. a[i]>0
	int R = 1e9; //За 1е9 ударов точно можно, т.к. p>q>=1 и a[i]<=1e9 
	while (L + 1 < R) { // Бинпоиск по ответу
		int M = (L + R) / 2;
		if (couldBeSolved(M, a, n, p - q, q)) {
			R = M;
		} else {
			L = M;
		}
	}
	return R;
}

int main(){
	int n = 0, p = 0, q = 0;
	cin >> n >> p >> q;
	int* a = new int[n];
	for (int i = 0; i < n; ++i) {
		cin >> a[i];
	}
	cout << binSearch(a, n, p, q);
	free(a);
	return 0;
}
