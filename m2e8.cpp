#include <iostream>

/*
Задача - написать LSD сортировку массива.
Решение - реализация.
*/

void sort(long long* arr, int size) {
	long long* buf = new long long[size];
	int blockSize = 8;
	int typeSize = 64;
	int blockCount = typeSize / blockSize;
	int mask = (1 << blockSize) - 1;
	int* count = new int[1 << blockSize];
	for (int bl = 0, shift = 0; bl < blockCount; shift += blockSize, ++bl) {
		for (int i = 0; i < (1<<blockSize); ++i) {
			count[i] = 0;
		}
		for (int i = 0; i < size; ++i) {
			++count[(arr[i] >> shift) & mask];
		}
		for (int i = 1; i < (1 << blockSize); ++i) {
			count[i] += count[i - 1];
		}
		for (int i = size - 1; i >= 0; --i) {
			buf[--count[(arr[i] >> shift) & mask]] = arr[i];
		}
		for (int i = 0; i < size; ++i) {
			arr[i] = buf[i];
		}
	}
	delete[] buf;
	delete[] count;
}

int main(){
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0;
	std::cin >> n;
	long long* arr =  new long long[n];
	for (int i = 0; i < n; ++i) {
		std::cin >> arr[i];
	}
	sort(arr, n);
	for (int i = 0; i < n; ++i) {
		std::cout << arr[i] << " ";
	}
	delete[] arr;
}
