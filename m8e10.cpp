#include <iostream>
#include <vector>
#include <algorithm>

/*
В состав Галактической империи входит N планет. Между большинством из них существуют гиперканалы. Новый император повелел, чтобы с любой планеты можно было попасть на любую другую, пройдя только через один гиперканал. Каналы устроены так, что позволяют путешествовать только в одну сторону. Единственный оставшийся прокладчик гиперканалов расположен на базе около планеты с номером A. К сожалению, он не может путешествовать по уже существующим каналам, он всегда прокладывает новый. А наличие двух каналов в одном направлении между двумя планетами существенно осложняет навигацию. Ваша задача – найти такой маршрут для прокладчика, чтобы все необходимые каналы были построены, и не было бы построено лишних. В конце своего маршрута прокладчик должен оказаться на своей родной базе, с которой он начал движение.

Входные данные
В первой строке находится число N≤1000 и число A≤N. N следующих строк содержит по N чисел: в i-й строке j-е число равно 1, если есть канал от планеты i к планете j, иначе 0. Известно, что Галактическая империя может удовлетворить свою потребность в гиперканалах прокладкой не более чем 32000 новых каналов.

Выходные данные
Выведите последовательность, в которой следует прокладывать каналы. Каждая строка должна содержать два целых числа: номера планет, с какой и на какую следует проложить очередной гиперканал. Гиперканалы следует перечислить в порядке их прокладки. Гарантируется, что решение существует.

Решение:
Цель - найти Эйлеров цикл в графе обратном данному.
Реализация стандартного алгоритма.
*/

class Graph{
public:
    explicit Graph(int n): edges(n, std::vector<int>()) {}
    void addEdge(int a, int b) {
        edges[a].push_back(b);
    } 
    const std::vector<int>& getNeighbours(int v) const {
        return edges[v];
    }
    int getSize() const {
        return edges.size();
    }
private:
    std::vector<std::vector<int>> edges; 
};

class GraphProcessor {
public:
    explicit GraphProcessor(const Graph& graph): graph(graph), ptr(graph.getSize(), 0),
                                            paths(graph.getSize(), std::vector<int>()) {}
    void returnEuler(int start, std::vector<std::pair<int,int>>& ans) {
        std::vector<int> order;
        order.reserve(graph.getSize());
        order.push_back(start);
        for (int i = 0; i < graph.getSize(); i++) {
            if (i != start) {
                order.push_back(i);
            }
        }
        for (auto v: order) {
            startSearch(v, v);
        }
        calculateEuler(start, ans);
    } 
private:
    const Graph& graph;
    std::vector<int> ptr;
    std::vector<std::vector<int>> paths;
    void startSearch(int v, int startV) {
        if (ptr[v] == graph.getNeighbours(v).size()) {
            return;
        }
        paths[startV].push_back(v);
        ptr[v]++; 
        startSearch(graph.getNeighbours(v)[ptr[v] - 1], startV);
    }
    void calculateEuler(int startV, std::vector<std::pair<int,int>>& ans) {
        std::vector<bool> used(graph.getSize(), 0);
        used[startV] = 1;
        std::vector<int> path;
        path.reserve(graph.getSize() * graph.getSize());
        for (auto v: paths[startV]) {    
            if (!used[v]) {
                used[v] = true;
                for (auto t: paths[v]) {
                    path.push_back(t);
                }
            }
            path.push_back(v);
        }
        path.push_back(startV);

        ans.reserve(path.size());
        for (int i = 1; i < path.size(); i++) {
            ans.push_back({path[i - 1], path[i]});
        }
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    int n = 0, start = 0;
    std::cin >> n >> start;
    start--;
    Graph g(n); 
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            bool input;
            std::cin >> input;
            if (!input && i != j) {
                g.addEdge(i, j);
            }
        }
    }
    std::vector<std::pair<int,int>> ans;
    GraphProcessor(g).returnEuler(start, ans);
    for (auto t: ans) {
        std::cout << t.first + 1 << " " << t.second + 1 << "\n";
    }
}
