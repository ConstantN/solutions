#include <iostream>
#include <vector>
#include <cmath>
#include <set>

/*
Задача:
Дано число N и последовательность из N целых чисел. Найти вторую порядковую статистику на заданных диапазонах.

Для решения задачи используйте структуру данных Sparse Table. Требуемое время обработки каждого диапазона O(1). Время подготовки структуры данных O(nlogn).

Входные данные
В первой строке заданы 2 числа: размер последовательности N и количество диапазонов M.

Следующие N целых чисел задают последовательность. Далее вводятся M пар чисел - границ диапазонов.

Решение - спарс тэйбл, в вершине храним индекс первого и второго минимума на отрезке.

Чтобы получить объединение отрезков - просто добавим все индексы в сет и отсортируем по значениям элементов
затем возьмём первый и второй элементы по возрастанию в качестве первого и второго минимума.
*/

class Comparator {
public:
	static std::vector<int> v;
	bool operator()(int i, int j) {
		return v[i] < v[j];
	}
};
std::vector<int> Comparator::v;

class SparseTable {
public:
	SparseTable(const std::vector<int>& v): v(v) {
		countDegrees();
		buildTable();
	}
	int getSecondMin(int l, int r) const;
private:
	const std::vector<int>& v;
	std::vector<int> degree;
	std::vector<std::vector<std::pair<int,int>>> table;
	void countDegrees();
	void buildTable();
};

void SparseTable::countDegrees() {
	degree.resize(v.size() + 1);
	for (int i = 0; i < int(degree.size()); ++i) {
		int N = i;
		while (N > 1) {
			N /= 2;
			++degree[i];
		}
	}
}

void SparseTable::buildTable() {
	int n = v.size();
	table.resize(n, std::vector<std::pair<int,int>>(degree[n] + 1));	
	for (int i = 0; i < n; ++i) {
		table[i][0] = {i, n - 1};
	}
	for (int i = 1; i < degree[n] + 1; ++i) {
		for (int j = 0; j < n; ++j) {
			if (j + (1 << i) > n)
				break;
			int index2 = j + (1 << (i - 1));
			std::set<int, Comparator> indexes = {table[j][i-1].first, table[j][i-1].second, table[index2][i-1].first, table[index2][i-1].second};
			auto t = indexes.begin();
			table[j][i].first = *t;
			++t;
			table[j][i].second = *t;
		}
	}
}

int SparseTable::getSecondMin(int l, int r) const {
	int lg = degree[r - l + 1];
	std::set<int, Comparator> indexes = {table[l][lg].first, table[l][lg].second, table[r - (1 << lg) + 1][lg].first, table[r - (1 << lg) + 1][lg].second};
	auto t = indexes.begin();
	++t;
	return v[*t];
}

main(){
	std::ios::sync_with_stdio(false);
	std::cin.tie(0);
	int n = 0, m = 0;
	std::cin >> n >> m;
	std::vector<int> v(n);
	for (auto &t: v)
		std::cin >> t;
	v.push_back(2 * 1e9 + 1);
	Comparator::v = v;
	SparseTable table = SparseTable(v);
	
	while (m--) {
		int l = 0, r = 0;
		std::cin >> l >> r;
		--l, --r;
		std::cout << table.getSecondMin(l, r) << "\n";
	}
}
