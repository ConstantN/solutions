#include <iostream>

using std::cin;
using std::cout;

/*
Реализуйте splay-дерево, которое поддерживает множество S целых чисел, в котором разрешается производить следующие операции:

add(i) — добавить в множество S число i (если он там уже есть, то множество не меняется);
sum(l,r) — вывести сумму всех элементов x из S, которые удовлетворяют неравенству l≤x≤r.
*/

struct Node {
    int value;
    long long sum;
    Node* l;
    Node* r;
    Node(int value): value(value), sum(value), l(nullptr), r(nullptr){}
    static long long getSum(Node* node);
    static void updateSum(Node* node);
    ~Node() {
	delete l;
	delete r;
    }
};

long long Node::getSum(Node* node) {
    if (!node)
        return 0;
    return node->sum;
}

void Node::updateSum(Node* node) {
    if (!node)
        return;
    node->sum = getSum(node->l) + getSum(node->r) + node->value;
}

class SplayTree{
public:
    SplayTree(): root(nullptr){}
    void add(int value);
    long long sum(int l, int r);
    ~SplayTree() {
	delete root;
    }
private:
    Node* root;
    static Node* insert(Node* node, int value);
    static long long getSegmentSum(Node*& node, int l, int r);
    static Node* rotateLeft(Node* node);
    static Node* rotateRight(Node* node);
    static Node* splay(Node* node, int value);
    static std::pair<Node*, Node*> split(Node* node, int k);
    static Node* merge(Node* a, Node* b);
    static bool find(Node* node, int value);
    static int findMax(Node* node);
};

void SplayTree::add(int value) {
    root = insert(root, value);
}

long long SplayTree::sum(int l, int r) {
    return getSegmentSum(root, l, r);
}

Node* SplayTree::insert(Node* node, int value) {
    if (find(node, value))
        return node;
    auto t = split(node, value);
    return merge(t.first, merge(new Node(value), t.second));
}

long long SplayTree::getSegmentSum(Node*& node, int l, int r) {
    auto t = split(node, l - 1);
    auto t1 = split(t.second, r);
    long long ans = Node::getSum(t1.first);
    node = merge(t.first, merge(t1.first, t1.second));
    return ans;
}

bool SplayTree::find(Node* node, int value) {
    if (!node)
        return false;
    if (node->value == value)
        return true;
    if (node->value < value)
        return find(node->r, value);
    return find(node->l, value);
}

std::pair<Node*, Node*> SplayTree::split(Node* node, int k) {
    if (!node)
        return {nullptr, nullptr};
    if (node->value <= k) {
        auto t = split(node->r, k);
        node->r = t.first;
        Node::updateSum(node);
        return {node, t.second};
    } else {
        auto t = split(node->l, k);
        node->l = t.second;
        Node::updateSum(node);
        return {t.first, node};
    }
}

int SplayTree::findMax(Node* node) {
    if (!node->r)
        return node->value;
    return findMax(node->r);
}

Node* SplayTree::merge(Node* a, Node* b) {
    if (!a)
        return b;
    a = splay(a, findMax(a));
    a->r = b;
    Node::updateSum(a);
    return a;
}

Node* SplayTree::rotateLeft(Node* node) {
    Node* top = node->r;
    node->r = top->l;
    top->l = node;
    Node::updateSum(node);
    Node::updateSum(top);
    return top;
}

Node* SplayTree::rotateRight(Node* node) {
    Node* top = node->l;
    node->l = top->r;
    top->r = node;
    Node::updateSum(node);
    Node::updateSum(top);
    return top;
}

Node* SplayTree::splay(Node* node, int value) {
    if (!node || node->value == value)
        return node;
    if (value < node->value) {
        node->l = splay(node->l, value);
        node = rotateRight(node);
    } else {
        node->r = splay(node->r, value);
        node = rotateLeft(node);
    }
    Node::updateSum(node);
    return node;
}

main() {
    std::ios::sync_with_stdio(false);
    cin.tie(0);
    const long long INF = 1e18;
    const long long MOD = 1e9;

    int n = 0;
    cin >> n;
    SplayTree tree = SplayTree();
    long long last = INF;
    while (n--) {
        char c = '\n';
        cin >> c;
        if (c == '+') {
            int value = 0;
            cin >> value;
            if (last != INF)
                value = (last + value) % MOD;
            tree.add(value);
            last = INF;
        } else{
            int l = 0, r = 0;
            cin >> l >> r;
            last = tree.sum(l, r);
            cout << last << "\n";
        }
    }
}
