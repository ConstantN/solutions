/*
Сдача теоретических задач.
Предисловие: В данном файле приведены решения задач из теоретического листочка №1.
Для каждой задачи сначала написано небольшое описание алгоритма, затем идейная часть кода на C++.
Для простоты подразумевается что используется пространство имён std;

1. T(n) = 3T(sqrt(n)) + log(2,n)
Ответ: T(n) = O(log(2,n)^log(2,3))
Доказательство данного факта по определению(аналогично моему решению №2) слишком обширное, чтобы его приводить, других методов не знаю.


2. T(n) = 2T(n/2) + n*log(2,n)
bit.ly/34kVKCq


3. Рассуждения строятся в предположении, что в задаче имелось ввиду сначала купить акции по какой-то цене, а затем продать с целью получить наибольшую выгоду.
Тогда цель - получить наибольшее значение выражения a[j]/a[i] , где i<j
Пусть мы решили продать в j-ый день, тогда оптимально будет купить по цене min(a[i]) по всем i(таким что i<j).
Тогда просто будем поддерживать минимум на префиксе(в переменной minOnPref) и обновлять ответ для каждого i.
Код:

int minOnPref = a[0];
double maxRatio = -1;
for(int i = 1; i < n; i++){
	maxRatio = max(maxRatio, 1.0*a[i]/minOnPref);
	minOnPref = min(minOnPref, a[i]);
}
//ans = maxRatio;


4.
Идея - сканлайн. Будем идти по массиву слева направо и поддерживать
1) сумму которую мы добавляем: sumAdd
2) сумму d[i] всех прогрессий, на отрезках которых мы сейчас находимся: sumDelta

Тогда на каждом шаге нужно:
sumAdd += sumDelta
a[i] += sumAdd

Код:

vector< vector< pair<int,int> > > events(n+1,vector<pair<int,int>>());
for(int i = 0; i < q; i++){
	l[i]--;
	r[i]--;
	events[l[i]].push_back({i,1}); //событие открытия арифм. последовательности
	events[r[i]+1].push_back({i,0}); //событие закрытия арифм. последовательности
}

int sumAdd = 0;
int sumDelta = 0;
for(int i = 0; i < n; i++){
	for(auto t:events[i]){ //обновляем sumAdd и sumDelta по новым пришедшим прогрессиям и вышедшим
		if(t.second==1) sumAdd += b[t.first];
		if(t.second==0){
			sumDelta -= d[t.first];
			sumAdd -= b[t.first] + (r[t.first] - l[t.first])*d[t.first];
		}
	}
	sumAdd += sumDelta;
	for(auto t:events[i]){ //обновляем sumDelta по пришедшим прогрессиям отдельно(после sumAdd+=sumDelta), ибо первый член прогрессии = b[i], а не b[i]+d[i];
		if(t.second==1) sumDelta += d[t.first];
	}

	a[i] += sumAdd;
}

for(int i = 0; i < n; i++) cout<<a[i]<<" ";

5.
Используем подобие бинпоиска.
Будем поддерживать L на значениях одного типа
R - на значениях другого типа.
Код:

int L = 0;
int R = n-1;
while(L+1<R){
	int M = (L+R)/2;
	if(v[M]==v[L]){
		L = M;
	}else{
		R = M;
	}
}
cout<<L<<" "<<R;


6.
Используем идею схожую с задачей 3. Только теперь создадим вспомогательный массив maxOnPrefA,
где maxOnPref[i] - максимум на префиксе массива A длиной i+1;
Затем пойдём с конца массива B и будем поддерживать maxOnSufB - максимум на суффиксе массива B.
Код:

vector<int> maxOnPrefA(n);
maxOnPrefA[0] = a[0];
for(int i = 1; i < n; i++){
	maxOnPrefA[i] = max(maxOnPrefA[i-1], a[i]);
}
int maxOnSufB = -1e9;
int Ans = -2*1e9;
for(int i = n-1; i >= 0; i--){
	maxOnSufB = max(maxOnSufB, b[i]);
	Ans = max(Ans, maxOnSufB + maxOnPrefA[i]);
}
cout<<Ans;

7.
Используем идею двух указателей. Будем перебирать элементы A слева направо(в порядке возрастания) 
и поддерживать указатель ptrB на элемент b такой, что a[ptrA]+b[ptrB]<k.
Код:

int ptrB = n-1;
int Ans = 0;
for(int ptrA = 0; ptrA < n-1; ptrA++){
	while(ptrB>0 && a[ptrA]+b[ptrB]>=k){
		Ans += (a[ptrA]+b[ptrB]==k);
		ptrB--;
	}
}
cout<<Ans;

*/
